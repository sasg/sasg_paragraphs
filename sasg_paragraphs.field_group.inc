<?php
/**
 * @file
 * sasg_paragraphs.field_group.inc
 */

/**
 * Implements hook_field_group_info().
 */
function sasg_paragraphs_field_group_info() {
  $field_groups = array();

  $field_group = new stdClass();
  $field_group->disabled = TRUE; /* Edit this to true to make a default field_group disabled initially */
  $field_group->api_version = 1;
  $field_group->identifier = 'group_card_block|paragraphs_item|sasg_card|default';
  $field_group->group_name = 'group_card_block';
  $field_group->entity_type = 'paragraphs_item';
  $field_group->bundle = 'sasg_card';
  $field_group->mode = 'default';
  $field_group->parent_name = 'group_card';
  $field_group->data = array(
    'label' => 'Card Block',
    'weight' => '9',
    'children' => array(
      0 => 'field_sasg_card_links',
      1 => 'group_card_text',
    ),
    'format_type' => 'html-element',
    'format_settings' => array(
      'label' => 'Card Block',
      'instance_settings' => array(
        'id' => '',
        'classes' => 'card-block',
        'element' => 'div',
        'show_label' => '0',
        'label_element' => 'div',
        'attributes' => '',
      ),
    ),
  );
  $field_groups['group_card_block|paragraphs_item|sasg_card|default'] = $field_group;

  $field_group = new stdClass();
  $field_group->disabled = FALSE; /* Edit this to true to make a default field_group disabled initially */
  $field_group->api_version = 1;
  $field_group->identifier = 'group_card_block|paragraphs_item|sasg_card|sasg_multiple_paragraphs';
  $field_group->group_name = 'group_card_block';
  $field_group->entity_type = 'paragraphs_item';
  $field_group->bundle = 'sasg_card';
  $field_group->mode = 'sasg_multiple_paragraphs';
  $field_group->parent_name = 'group_card';
  $field_group->data = array(
    'label' => 'Card Block',
    'weight' => '9',
    'children' => array(
      0 => 'field_sasg_card_links',
      1 => 'group_card_text',
    ),
    'format_type' => 'html-element',
    'format_settings' => array(
      'label' => 'Card Block',
      'instance_settings' => array(
        'id' => '',
        'classes' => 'card-block',
        'element' => 'div',
        'show_label' => '0',
        'label_element' => 'div',
        'attributes' => '',
      ),
    ),
  );
  $field_groups['group_card_block|paragraphs_item|sasg_card|sasg_multiple_paragraphs'] = $field_group;

  $field_group = new stdClass();
  $field_group->disabled = FALSE; /* Edit this to true to make a default field_group disabled initially */
  $field_group->api_version = 1;
  $field_group->identifier = 'group_card_deck_wrapper|paragraphs_item|sasg_card_deck|default';
  $field_group->group_name = 'group_card_deck_wrapper';
  $field_group->entity_type = 'paragraphs_item';
  $field_group->bundle = 'sasg_card_deck';
  $field_group->mode = 'default';
  $field_group->parent_name = '';
  $field_group->data = array(
    'label' => 'Card deck wrapper',
    'weight' => '0',
    'children' => array(
      0 => 'group_card_deck',
    ),
    'format_type' => 'html-element',
    'format_settings' => array(
      'label' => 'Card deck wrapper',
      'instance_settings' => array(
        'id' => '',
        'classes' => 'row bottom-buffer-30',
        'element' => 'div',
        'show_label' => '0',
        'label_element' => 'div',
        'attributes' => '',
      ),
    ),
  );
  $field_groups['group_card_deck_wrapper|paragraphs_item|sasg_card_deck|default'] = $field_group;

  $field_group = new stdClass();
  $field_group->disabled = FALSE; /* Edit this to true to make a default field_group disabled initially */
  $field_group->api_version = 1;
  $field_group->identifier = 'group_card_deck|paragraphs_item|sasg_card_deck|default';
  $field_group->group_name = 'group_card_deck';
  $field_group->entity_type = 'paragraphs_item';
  $field_group->bundle = 'sasg_card_deck';
  $field_group->mode = 'default';
  $field_group->parent_name = 'group_card_deck_wrapper';
  $field_group->data = array(
    'label' => 'Card Deck',
    'weight' => '1',
    'children' => array(
      0 => 'field_sasg_cards',
    ),
    'format_type' => 'html-element',
    'format_settings' => array(
      'label' => 'Card Deck',
      'instance_settings' => array(
        'id' => '',
        'classes' => 'card-deck',
        'element' => 'div',
        'show_label' => '0',
        'label_element' => 'div',
        'attributes' => '',
      ),
    ),
  );
  $field_groups['group_card_deck|paragraphs_item|sasg_card_deck|default'] = $field_group;

  $field_group = new stdClass();
  $field_group->disabled = TRUE; /* Edit this to true to make a default field_group disabled initially */
  $field_group->api_version = 1;
  $field_group->identifier = 'group_card_header|paragraphs_item|sasg_card|default';
  $field_group->group_name = 'group_card_header';
  $field_group->entity_type = 'paragraphs_item';
  $field_group->bundle = 'sasg_card';
  $field_group->mode = 'default';
  $field_group->parent_name = 'group_card';
  $field_group->data = array(
    'label' => 'Card Header',
    'weight' => '7',
    'children' => array(
      0 => 'group_card_title',
    ),
    'format_type' => 'html-element',
    'format_settings' => array(
      'label' => 'Card Header',
      'instance_settings' => array(
        'id' => '',
        'classes' => 'card-header',
        'element' => 'div',
        'show_label' => '0',
        'label_element' => 'div',
        'attributes' => '',
      ),
    ),
  );
  $field_groups['group_card_header|paragraphs_item|sasg_card|default'] = $field_group;

  $field_group = new stdClass();
  $field_group->disabled = FALSE; /* Edit this to true to make a default field_group disabled initially */
  $field_group->api_version = 1;
  $field_group->identifier = 'group_card_header|paragraphs_item|sasg_card|sasg_multiple_paragraphs';
  $field_group->group_name = 'group_card_header';
  $field_group->entity_type = 'paragraphs_item';
  $field_group->bundle = 'sasg_card';
  $field_group->mode = 'sasg_multiple_paragraphs';
  $field_group->parent_name = 'group_card';
  $field_group->data = array(
    'label' => 'Card Header',
    'weight' => '7',
    'children' => array(
      0 => 'group_card_title',
    ),
    'format_type' => 'html-element',
    'format_settings' => array(
      'label' => 'Card Header',
      'instance_settings' => array(
        'id' => '',
        'classes' => 'card-header',
        'element' => 'div',
        'show_label' => '0',
        'label_element' => 'div',
        'attributes' => '',
      ),
    ),
  );
  $field_groups['group_card_header|paragraphs_item|sasg_card|sasg_multiple_paragraphs'] = $field_group;

  $field_group = new stdClass();
  $field_group->disabled = TRUE; /* Edit this to true to make a default field_group disabled initially */
  $field_group->api_version = 1;
  $field_group->identifier = 'group_card_text|paragraphs_item|sasg_card|default';
  $field_group->group_name = 'group_card_text';
  $field_group->entity_type = 'paragraphs_item';
  $field_group->bundle = 'sasg_card';
  $field_group->mode = 'default';
  $field_group->parent_name = 'group_card_block';
  $field_group->data = array(
    'label' => 'Card Text',
    'weight' => '5',
    'children' => array(
      0 => 'field_sasg_card_summary',
    ),
    'format_type' => 'html-element',
    'format_settings' => array(
      'label' => 'Card Text',
      'instance_settings' => array(
        'id' => '',
        'classes' => 'card-text',
        'element' => 'p',
        'show_label' => '0',
        'label_element' => 'div',
        'attributes' => '',
      ),
    ),
  );
  $field_groups['group_card_text|paragraphs_item|sasg_card|default'] = $field_group;

  $field_group = new stdClass();
  $field_group->disabled = FALSE; /* Edit this to true to make a default field_group disabled initially */
  $field_group->api_version = 1;
  $field_group->identifier = 'group_card_text|paragraphs_item|sasg_card|sasg_multiple_paragraphs';
  $field_group->group_name = 'group_card_text';
  $field_group->entity_type = 'paragraphs_item';
  $field_group->bundle = 'sasg_card';
  $field_group->mode = 'sasg_multiple_paragraphs';
  $field_group->parent_name = 'group_card_block';
  $field_group->data = array(
    'label' => 'Card Text',
    'weight' => '13',
    'children' => array(
      0 => 'field_sasg_card_summary',
    ),
    'format_type' => 'html-element',
    'format_settings' => array(
      'label' => 'Card Text',
      'instance_settings' => array(
        'id' => '',
        'classes' => 'card-text',
        'element' => 'div',
        'show_label' => '0',
        'label_element' => 'div',
        'attributes' => '',
      ),
    ),
  );
  $field_groups['group_card_text|paragraphs_item|sasg_card|sasg_multiple_paragraphs'] = $field_group;

  $field_group = new stdClass();
  $field_group->disabled = TRUE; /* Edit this to true to make a default field_group disabled initially */
  $field_group->api_version = 1;
  $field_group->identifier = 'group_card_title|paragraphs_item|sasg_card|default';
  $field_group->group_name = 'group_card_title';
  $field_group->entity_type = 'paragraphs_item';
  $field_group->bundle = 'sasg_card';
  $field_group->mode = 'default';
  $field_group->parent_name = 'group_card_header';
  $field_group->data = array(
    'label' => 'Card Title',
    'weight' => '8',
    'children' => array(
      0 => 'field_sasg_card_title',
    ),
    'format_type' => 'html-element',
    'format_settings' => array(
      'label' => 'Card Title',
      'instance_settings' => array(
        'id' => '',
        'classes' => 'card-title',
        'element' => 'div',
        'show_label' => '0',
        'label_element' => 'div',
        'attributes' => '',
      ),
    ),
  );
  $field_groups['group_card_title|paragraphs_item|sasg_card|default'] = $field_group;

  $field_group = new stdClass();
  $field_group->disabled = FALSE; /* Edit this to true to make a default field_group disabled initially */
  $field_group->api_version = 1;
  $field_group->identifier = 'group_card_title|paragraphs_item|sasg_card|sasg_multiple_paragraphs';
  $field_group->group_name = 'group_card_title';
  $field_group->entity_type = 'paragraphs_item';
  $field_group->bundle = 'sasg_card';
  $field_group->mode = 'sasg_multiple_paragraphs';
  $field_group->parent_name = 'group_card_header';
  $field_group->data = array(
    'label' => 'Card Title',
    'weight' => '6',
    'children' => array(
      0 => 'field_sasg_card_title',
    ),
    'format_type' => 'html-element',
    'format_settings' => array(
      'label' => 'Card Title',
      'instance_settings' => array(
        'id' => '',
        'classes' => 'card-title',
        'element' => 'div',
        'show_label' => '0',
        'label_element' => 'div',
        'attributes' => '',
      ),
    ),
  );
  $field_groups['group_card_title|paragraphs_item|sasg_card|sasg_multiple_paragraphs'] = $field_group;

  $field_group = new stdClass();
  $field_group->disabled = TRUE; /* Edit this to true to make a default field_group disabled initially */
  $field_group->api_version = 1;
  $field_group->identifier = 'group_card|paragraphs_item|sasg_card|default';
  $field_group->group_name = 'group_card';
  $field_group->entity_type = 'paragraphs_item';
  $field_group->bundle = 'sasg_card';
  $field_group->mode = 'default';
  $field_group->parent_name = '';
  $field_group->data = array(
    'label' => 'Card',
    'weight' => '0',
    'children' => array(
      0 => 'field_sasg_card_photo',
      1 => 'group_card_header',
      2 => 'group_card_block',
    ),
    'format_type' => 'html-element',
    'format_settings' => array(
      'label' => 'Card',
      'instance_settings' => array(
        'id' => '',
        'classes' => 'card',
        'element' => 'div',
        'show_label' => '0',
        'label_element' => 'div',
        'attributes' => '',
      ),
    ),
  );
  $field_groups['group_card|paragraphs_item|sasg_card|default'] = $field_group;

  $field_group = new stdClass();
  $field_group->disabled = FALSE; /* Edit this to true to make a default field_group disabled initially */
  $field_group->api_version = 1;
  $field_group->identifier = 'group_card|paragraphs_item|sasg_card|sasg_multiple_paragraphs';
  $field_group->group_name = 'group_card';
  $field_group->entity_type = 'paragraphs_item';
  $field_group->bundle = 'sasg_card';
  $field_group->mode = 'sasg_multiple_paragraphs';
  $field_group->parent_name = '';
  $field_group->data = array(
    'label' => 'Card',
    'weight' => '0',
    'children' => array(
      0 => 'field_sasg_card_photo',
      1 => 'group_card_header',
      2 => 'group_card_block',
    ),
    'format_type' => 'html-element',
    'format_settings' => array(
      'label' => 'Card',
      'instance_settings' => array(
        'id' => '',
        'classes' => 'card',
        'element' => 'div',
        'show_label' => '0',
        'label_element' => 'div',
        'attributes' => '',
      ),
    ),
  );
  $field_groups['group_card|paragraphs_item|sasg_card|sasg_multiple_paragraphs'] = $field_group;

  $field_group = new stdClass();
  $field_group->disabled = FALSE; /* Edit this to true to make a default field_group disabled initially */
  $field_group->api_version = 1;
  $field_group->identifier = 'group_left|paragraphs_item|sasg_card|default';
  $field_group->group_name = 'group_left';
  $field_group->entity_type = 'paragraphs_item';
  $field_group->bundle = 'sasg_card';
  $field_group->mode = 'default';
  $field_group->parent_name = 'group_row';
  $field_group->data = array(
    'label' => 'Left',
    'weight' => '7',
    'children' => array(
      0 => 'field_sasg_card_photo',
    ),
    'format_type' => 'html-element',
    'format_settings' => array(
      'label' => 'Left',
      'instance_settings' => array(
        'id' => '',
        'classes' => 'col-sm-4',
        'element' => 'div',
        'show_label' => '0',
        'label_element' => 'div',
        'attributes' => '',
      ),
    ),
  );
  $field_groups['group_left|paragraphs_item|sasg_card|default'] = $field_group;

  $field_group = new stdClass();
  $field_group->disabled = FALSE; /* Edit this to true to make a default field_group disabled initially */
  $field_group->api_version = 1;
  $field_group->identifier = 'group_links|paragraphs_item|sasg_card|default';
  $field_group->group_name = 'group_links';
  $field_group->entity_type = 'paragraphs_item';
  $field_group->bundle = 'sasg_card';
  $field_group->mode = 'default';
  $field_group->parent_name = 'group_right';
  $field_group->data = array(
    'label' => 'Links',
    'weight' => '11',
    'children' => array(
      0 => 'field_sasg_card_links',
    ),
    'format_type' => 'html-element',
    'format_settings' => array(
      'formatter' => '',
      'instance_settings' => array(
        'element' => 'div',
        'show_label' => 0,
        'label_element' => 'div',
        'classes' => 'group-links field-group-html-element',
        'attributes' => '',
        'required_fields' => 1,
        'id' => '',
      ),
    ),
  );
  $field_groups['group_links|paragraphs_item|sasg_card|default'] = $field_group;

  $field_group = new stdClass();
  $field_group->disabled = FALSE; /* Edit this to true to make a default field_group disabled initially */
  $field_group->api_version = 1;
  $field_group->identifier = 'group_panel_group|paragraphs_item|sasg_panel_group|default';
  $field_group->group_name = 'group_panel_group';
  $field_group->entity_type = 'paragraphs_item';
  $field_group->bundle = 'sasg_panel_group';
  $field_group->mode = 'default';
  $field_group->parent_name = '';
  $field_group->data = array(
    'label' => 'Panel group',
    'weight' => '0',
    'children' => array(
      0 => 'field_sasg_panel_items',
    ),
    'format_type' => 'html-element',
    'format_settings' => array(
      'label' => 'Panel group',
      'instance_settings' => array(
        'id' => '',
        'classes' => 'panel-group',
        'element' => 'div',
        'show_label' => '0',
        'label_element' => 'div',
        'attributes' => 'role="tablist" aria-multiselectable="true"',
      ),
    ),
  );
  $field_groups['group_panel_group|paragraphs_item|sasg_panel_group|default'] = $field_group;

  $field_group = new stdClass();
  $field_group->disabled = FALSE; /* Edit this to true to make a default field_group disabled initially */
  $field_group->api_version = 1;
  $field_group->identifier = 'group_panel|paragraphs_item|sasg_panel_item|default';
  $field_group->group_name = 'group_panel';
  $field_group->entity_type = 'paragraphs_item';
  $field_group->bundle = 'sasg_panel_item';
  $field_group->mode = 'default';
  $field_group->parent_name = '';
  $field_group->data = array(
    'label' => 'Panel',
    'weight' => '0',
    'children' => array(
      0 => 'field_sasg_panel_heading',
      1 => 'field_sasg_panel_body',
    ),
    'format_type' => 'html-element',
    'format_settings' => array(
      'label' => 'Panel',
      'instance_settings' => array(
        'id' => '',
        'classes' => 'panel panel-default',
        'element' => 'div',
        'show_label' => '0',
        'label_element' => 'div',
        'attributes' => '',
      ),
    ),
  );
  $field_groups['group_panel|paragraphs_item|sasg_panel_item|default'] = $field_group;

  $field_group = new stdClass();
  $field_group->disabled = FALSE; /* Edit this to true to make a default field_group disabled initially */
  $field_group->api_version = 1;
  $field_group->identifier = 'group_right|paragraphs_item|sasg_card|default';
  $field_group->group_name = 'group_right';
  $field_group->entity_type = 'paragraphs_item';
  $field_group->bundle = 'sasg_card';
  $field_group->mode = 'default';
  $field_group->parent_name = 'group_row';
  $field_group->data = array(
    'label' => 'Right',
    'weight' => '8',
    'children' => array(
      0 => 'field_sasg_card_title',
      1 => 'field_sasg_card_summary',
      2 => 'group_links',
    ),
    'format_type' => 'html-element',
    'format_settings' => array(
      'label' => 'Right',
      'instance_settings' => array(
        'id' => '',
        'classes' => 'col-sm-8',
        'element' => 'div',
        'show_label' => '0',
        'label_element' => 'div',
        'attributes' => '',
      ),
    ),
  );
  $field_groups['group_right|paragraphs_item|sasg_card|default'] = $field_group;

  $field_group = new stdClass();
  $field_group->disabled = FALSE; /* Edit this to true to make a default field_group disabled initially */
  $field_group->api_version = 1;
  $field_group->identifier = 'group_row|paragraphs_item|sasg_card|default';
  $field_group->group_name = 'group_row';
  $field_group->entity_type = 'paragraphs_item';
  $field_group->bundle = 'sasg_card';
  $field_group->mode = 'default';
  $field_group->parent_name = '';
  $field_group->data = array(
    'label' => 'Row',
    'weight' => '0',
    'children' => array(
      0 => 'group_left',
      1 => 'group_right',
    ),
    'format_type' => 'html-element',
    'format_settings' => array(
      'label' => 'Row',
      'instance_settings' => array(
        'id' => '',
        'classes' => 'row bottom-buffer-30 single-card',
        'element' => 'div',
        'show_label' => '0',
        'label_element' => 'div',
        'attributes' => '',
      ),
    ),
  );
  $field_groups['group_row|paragraphs_item|sasg_card|default'] = $field_group;

  $field_group = new stdClass();
  $field_group->disabled = FALSE; /* Edit this to true to make a default field_group disabled initially */
  $field_group->api_version = 1;
  $field_group->identifier = 'group_sasg_carousel_caption|paragraphs_item|sasg_carousel_slide|default';
  $field_group->group_name = 'group_sasg_carousel_caption';
  $field_group->entity_type = 'paragraphs_item';
  $field_group->bundle = 'sasg_carousel_slide';
  $field_group->mode = 'default';
  $field_group->parent_name = '';
  $field_group->data = array(
    'label' => 'Caption',
    'weight' => '1',
    'children' => array(
      0 => 'field_sasg_carousel_caption',
    ),
    'format_type' => 'html-element',
    'format_settings' => array(
      'label' => 'Caption',
      'instance_settings' => array(
        'id' => '',
        'classes' => 'carousel-caption bg-trans-white text-blue text-center',
        'element' => 'div',
        'show_label' => '0',
        'label_element' => 'div',
        'attributes' => '',
      ),
    ),
  );
  $field_groups['group_sasg_carousel_caption|paragraphs_item|sasg_carousel_slide|default'] = $field_group;

  $field_group = new stdClass();
  $field_group->disabled = FALSE; /* Edit this to true to make a default field_group disabled initially */
  $field_group->api_version = 1;
  $field_group->identifier = 'group_sasg_carousel_inner|paragraphs_item|sasg_carousel|default';
  $field_group->group_name = 'group_sasg_carousel_inner';
  $field_group->entity_type = 'paragraphs_item';
  $field_group->bundle = 'sasg_carousel';
  $field_group->mode = 'default';
  $field_group->parent_name = '';
  $field_group->data = array(
    'label' => 'Carousel Inner',
    'weight' => '1',
    'children' => array(
      0 => 'field_sasg_carousel_slides',
    ),
    'format_type' => 'html-element',
    'format_settings' => array(
      'label' => 'Carousel Inner',
      'instance_settings' => array(
        'id' => '',
        'classes' => 'carousel-inner',
        'element' => 'div',
        'show_label' => '0',
        'label_element' => 'div',
        'attributes' => 'role="listbox',
      ),
    ),
  );
  $field_groups['group_sasg_carousel_inner|paragraphs_item|sasg_carousel|default'] = $field_group;

  // Translatables
  // Included for use with string extractors like potx.
  t('Caption');
  t('Card');
  t('Card Block');
  t('Card Deck');
  t('Card Header');
  t('Card Text');
  t('Card Title');
  t('Card deck wrapper');
  t('Carousel Inner');
  t('Left');
  t('Links');
  t('Panel');
  t('Panel group');
  t('Right');
  t('Row');

  return $field_groups;
}
