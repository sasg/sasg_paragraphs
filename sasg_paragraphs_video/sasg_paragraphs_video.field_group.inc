<?php
/**
 * @file
 * sasg_paragraphs_video.field_group.inc
 */

/**
 * Implements hook_field_group_info().
 */
function sasg_paragraphs_video_field_group_info() {
  $field_groups = array();

  $field_group = new stdClass();
  $field_group->disabled = FALSE; /* Edit this to true to make a default field_group disabled initially */
  $field_group->api_version = 1;
  $field_group->identifier = 'group_video_container|paragraphs_item|sasg_video|default';
  $field_group->group_name = 'group_video_container';
  $field_group->entity_type = 'paragraphs_item';
  $field_group->bundle = 'sasg_video';
  $field_group->mode = 'default';
  $field_group->parent_name = '';
  $field_group->data = array(
    'label' => 'Video container',
    'weight' => '3',
    'children' => array(
      0 => 'field_sssg_video',
    ),
    'format_type' => 'html-element',
    'format_settings' => array(
      'label' => 'Video container',
      'instance_settings' => array(
        'id' => '',
        'classes' => 'embed-responsive embed-responsive-16by9 bottom-buffer-30',
        'element' => 'div',
        'show_label' => '0',
        'label_element' => 'div',
        'attributes' => '',
      ),
    ),
  );
  $field_groups['group_video_container|paragraphs_item|sasg_video|default'] = $field_group;

  // Translatables
  // Included for use with string extractors like potx.
  t('Video container');

  return $field_groups;
}
