<?php
/**
 * @file
 * sasg_paragraphs.features.inc
 */

/**
 * Implements hook_ctools_plugin_api().
 */
function sasg_paragraphs_ctools_plugin_api($module = NULL, $api = NULL) {
  if ($module == "field_group" && $api == "field_group") {
    return array("version" => "1");
  }
  if ($module == "strongarm" && $api == "strongarm") {
    return array("version" => "1");
  }
  if ($module == "video_embed_field" && $api == "default_video_embed_styles") {
    return array("version" => "1");
  }
}

/**
 * Implements hook_image_default_styles().
 */
function sasg_paragraphs_image_default_styles() {
  $styles = array();

  // Exported image style: sasg_card.
  $styles['sasg_card'] = array(
    'label' => 'Card',
    'effects' => array(
      3 => array(
        'name' => 'focal_point_scale_and_crop',
        'data' => array(
          'width' => 568,
          'height' => 426,
          'focal_point_advanced' => array(
            'shift_x' => '',
            'shift_y' => '',
          ),
        ),
        'weight' => 1,
      ),
    ),
  );

  // Exported image style: sasg_card_horizontal.
  $styles['sasg_card_horizontal'] = array(
    'label' => 'Card horizontal',
    'effects' => array(
      4 => array(
        'name' => 'image_scale',
        'data' => array(
          'width' => 400,
          'height' => '',
          'upscale' => 1,
        ),
        'weight' => 1,
      ),
    ),
  );

  // Exported image style: sasg_carousel.
  $styles['sasg_carousel'] = array(
    'label' => 'Carousel',
    'effects' => array(
      5 => array(
        'name' => 'image_scale',
        'data' => array(
          'width' => 1140,
          'height' => '',
          'upscale' => 0,
        ),
        'weight' => 1,
      ),
    ),
  );

  return $styles;
}

/**
 * Implements hook_paragraphs_info().
 */
function sasg_paragraphs_paragraphs_info() {
  $items = array(
    'sasg_card' => array(
      'name' => 'Card',
      'bundle' => 'sasg_card',
      'locked' => '1',
    ),
    'sasg_card_deck' => array(
      'name' => 'Card Deck',
      'bundle' => 'sasg_card_deck',
      'locked' => '1',
    ),
    'sasg_carousel' => array(
      'name' => 'Carousel',
      'bundle' => 'sasg_carousel',
      'locked' => '1',
    ),
    'sasg_carousel_slide' => array(
      'name' => 'Carousel Slide',
      'bundle' => 'sasg_carousel_slide',
      'locked' => '1',
    ),
    'sasg_panel_group' => array(
      'name' => 'Panel group',
      'bundle' => 'sasg_panel_group',
      'locked' => '1',
    ),
    'sasg_panel_item' => array(
      'name' => 'Panel',
      'bundle' => 'sasg_panel_item',
      'locked' => '1',
    ),
    'sasg_tab_item' => array(
      'name' => 'Tab Section',
      'bundle' => 'sasg_tab_item',
      'locked' => '1',
    ),
    'sasg_tab_set' => array(
      'name' => 'Tabs',
      'bundle' => 'sasg_tab_set',
      'locked' => '1',
    ),
    'sasg_table' => array(
      'name' => 'Table',
      'bundle' => 'sasg_table',
      'locked' => '1',
    ),
    'sasg_text_field' => array(
      'name' => 'Text',
      'bundle' => 'sasg_text_field',
      'locked' => '1',
    ),
  );
  return $items;
}
