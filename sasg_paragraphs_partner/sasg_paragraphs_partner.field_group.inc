<?php
/**
 * @file
 * sasg_paragraphs_partner.field_group.inc
 */

/**
 * Implements hook_field_group_info().
 */
function sasg_paragraphs_partner_field_group_info() {
  $field_groups = array();

  $field_group = new stdClass();
  $field_group->disabled = FALSE; /* Edit this to true to make a default field_group disabled initially */
  $field_group->api_version = 1;
  $field_group->identifier = 'group_partner|paragraphs_item|sasg_partner|default';
  $field_group->group_name = 'group_partner';
  $field_group->entity_type = 'paragraphs_item';
  $field_group->bundle = 'sasg_partner';
  $field_group->mode = 'default';
  $field_group->parent_name = '';
  $field_group->data = array(
    'label' => 'Partner',
    'weight' => '0',
    'children' => array(
      0 => 'field_sasg_partner_logo',
    ),
    'format_type' => 'html-element',
    'format_settings' => array(
      'label' => 'Partner',
      'instance_settings' => array(
        'id' => '',
        'classes' => 'partner-logo col-sm-6 col-md-3',
        'element' => 'div',
        'show_label' => '0',
        'label_element' => 'div',
        'attributes' => '',
      ),
    ),
  );
  $field_groups['group_partner|paragraphs_item|sasg_partner|default'] = $field_group;

  $field_group = new stdClass();
  $field_group->disabled = FALSE; /* Edit this to true to make a default field_group disabled initially */
  $field_group->api_version = 1;
  $field_group->identifier = 'group_row|paragraphs_item|sasg_partner_group|default';
  $field_group->group_name = 'group_row';
  $field_group->entity_type = 'paragraphs_item';
  $field_group->bundle = 'sasg_partner_group';
  $field_group->mode = 'default';
  $field_group->parent_name = '';
  $field_group->data = array(
    'label' => 'Row',
    'weight' => '0',
    'children' => array(
      0 => 'field_sasg_partners',
    ),
    'format_type' => 'html-element',
    'format_settings' => array(
      'label' => 'Row',
      'instance_settings' => array(
        'id' => '',
        'classes' => 'row',
        'element' => 'div',
        'show_label' => '0',
        'label_element' => 'div',
        'attributes' => '',
      ),
    ),
  );
  $field_groups['group_row|paragraphs_item|sasg_partner_group|default'] = $field_group;

  // Translatables
  // Included for use with string extractors like potx.
  t('Partner');
  t('Row');

  return $field_groups;
}
