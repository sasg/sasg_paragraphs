<?php
/**
 * @file
 * sasg_paragraphs_partner.features.field_instance.inc
 */

/**
 * Implements hook_field_default_field_instances().
 */
function sasg_paragraphs_partner_field_default_field_instances() {
  $field_instances = array();

  // Exported field_instance:
  // 'paragraphs_item-sasg_partner-field_sasg_partner_link'.
  $field_instances['paragraphs_item-sasg_partner-field_sasg_partner_link'] = array(
    'bundle' => 'sasg_partner',
    'default_value' => NULL,
    'deleted' => 0,
    'description' => '',
    'display' => array(
      'default' => array(
        'label' => 'hidden',
        'settings' => array(),
        'type' => 'hidden',
        'weight' => 2,
      ),
      'paragraphs_editor_preview' => array(
        'label' => 'above',
        'settings' => array(),
        'type' => 'hidden',
        'weight' => 0,
      ),
    ),
    'entity_type' => 'paragraphs_item',
    'fences_wrapper' => 'no_wrapper',
    'field_name' => 'field_sasg_partner_link',
    'label' => 'Partner link',
    'required' => 1,
    'settings' => array(
      'absolute_url' => 1,
      'attributes' => array(
        'class' => '',
        'configurable_class' => 0,
        'configurable_title' => 0,
        'rel' => '',
        'target' => 'default',
        'title' => '',
      ),
      'display' => array(
        'url_cutoff' => 80,
      ),
      'enable_tokens' => 1,
      'rel_remove' => 'default',
      'title' => 'none',
      'title_label_use_field_label' => 0,
      'title_maxlength' => 128,
      'title_value' => '',
      'url' => 0,
      'user_register_form' => FALSE,
      'validate_url' => 1,
    ),
    'widget' => array(
      'active' => 0,
      'module' => 'link',
      'settings' => array(),
      'type' => 'link_field',
      'weight' => 2,
    ),
  );

  // Exported field_instance:
  // 'paragraphs_item-sasg_partner-field_sasg_partner_logo'.
  $field_instances['paragraphs_item-sasg_partner-field_sasg_partner_logo'] = array(
    'bundle' => 'sasg_partner',
    'deleted' => 0,
    'description' => '',
    'display' => array(
      'default' => array(
        'label' => 'hidden',
        'module' => 'image_link_formatter',
        'settings' => array(
          'image_link' => 'field_sasg_partner_link',
          'image_style' => 'sasg_partner_220w_150h',
        ),
        'type' => 'image_link_formatter',
        'weight' => 1,
      ),
      'paragraphs_editor_preview' => array(
        'label' => 'above',
        'settings' => array(),
        'type' => 'hidden',
        'weight' => 0,
      ),
    ),
    'entity_type' => 'paragraphs_item',
    'fences_wrapper' => 'no_wrapper',
    'field_name' => 'field_sasg_partner_logo',
    'label' => 'Partner logo',
    'required' => 1,
    'settings' => array(
      'alt_field' => 1,
      'default_image' => 0,
      'file_directory' => '',
      'file_extensions' => 'png gif jpg jpeg',
      'max_filesize' => '',
      'max_resolution' => '',
      'min_resolution' => '',
      'title_field' => 0,
      'user_register_form' => FALSE,
    ),
    'widget' => array(
      'active' => 1,
      'module' => 'image',
      'settings' => array(
        'preview_image_style' => 'thumbnail',
        'progress_indicator' => 'throbber',
      ),
      'type' => 'image_image',
      'weight' => 1,
    ),
  );

  // Exported field_instance:
  // 'paragraphs_item-sasg_partner_group-field_sasg_partners'.
  $field_instances['paragraphs_item-sasg_partner_group-field_sasg_partners'] = array(
    'bundle' => 'sasg_partner_group',
    'default_value' => NULL,
    'deleted' => 0,
    'description' => '',
    'display' => array(
      'default' => array(
        'label' => 'hidden',
        'module' => 'paragraphs',
        'settings' => array(
          'view_mode' => 'full',
        ),
        'type' => 'paragraphs_view',
        'weight' => 1,
      ),
      'paragraphs_editor_preview' => array(
        'label' => 'above',
        'settings' => array(),
        'type' => 'hidden',
        'weight' => 0,
      ),
    ),
    'entity_type' => 'paragraphs_item',
    'fences_wrapper' => 'no_wrapper',
    'field_name' => 'field_sasg_partners',
    'label' => 'Partners',
    'required' => 0,
    'settings' => array(
      'add_mode' => 'select',
      'allowed_bundles' => array(
        'sasg_card' => -1,
        'sasg_card_deck' => -1,
        'sasg_carousel' => -1,
        'sasg_carousel_slide' => -1,
        'sasg_panel_group' => -1,
        'sasg_panel_item' => -1,
        'sasg_partner' => 'sasg_partner',
        'sasg_partner_group' => -1,
        'sasg_photo_grid' => -1,
        'sasg_photo_grid_item' => -1,
        'sasg_statistic' => -1,
        'sasg_statistic_group' => -1,
        'sasg_tab_item' => -1,
        'sasg_tab_set' => -1,
        'sasg_table' => -1,
        'sasg_text_field' => -1,
        'sasg_video' => -1,
        'sasg_view' => -1,
      ),
      'bundle_weights' => array(
        'sasg_card' => 2,
        'sasg_card_deck' => 3,
        'sasg_carousel' => 4,
        'sasg_carousel_slide' => 5,
        'sasg_panel_group' => 6,
        'sasg_panel_item' => 7,
        'sasg_partner' => 8,
        'sasg_partner_group' => 9,
        'sasg_photo_grid' => 10,
        'sasg_photo_grid_item' => 11,
        'sasg_statistic' => 12,
        'sasg_statistic_group' => 13,
        'sasg_tab_item' => 15,
        'sasg_tab_set' => 16,
        'sasg_table' => 14,
        'sasg_text_field' => 17,
        'sasg_video' => 18,
        'sasg_view' => 19,
      ),
      'default_edit_mode' => 'open',
      'title' => 'Partner',
      'title_multiple' => 'Partners',
      'user_register_form' => FALSE,
    ),
    'widget' => array(
      'active' => 0,
      'module' => 'paragraphs',
      'settings' => array(),
      'type' => 'paragraphs_embed',
      'weight' => 1,
    ),
  );

  // Translatables
  // Included for use with string extractors like potx.
  t('Partner link');
  t('Partner logo');
  t('Partners');

  return $field_instances;
}
