<?php
/**
 * @file
 * sasg_paragraphs_partner.features.inc
 */

/**
 * Implements hook_ctools_plugin_api().
 */
function sasg_paragraphs_partner_ctools_plugin_api($module = NULL, $api = NULL) {
  if ($module == "field_group" && $api == "field_group") {
    return array("version" => "1");
  }
}

/**
 * Implements hook_image_default_styles().
 */
function sasg_paragraphs_partner_image_default_styles() {
  $styles = array();

  // Exported image style: sasg_partner_220w_150h.
  $styles['sasg_partner_220w_150h'] = array(
    'label' => 'partner 220w 150h',
    'effects' => array(
      6 => array(
        'name' => 'image_scale',
        'data' => array(
          'width' => 220,
          'height' => 150,
          'upscale' => 0,
        ),
        'weight' => 1,
      ),
    ),
  );

  return $styles;
}

/**
 * Implements hook_paragraphs_info().
 */
function sasg_paragraphs_partner_paragraphs_info() {
  $items = array(
    'sasg_partner' => array(
      'name' => 'Partner',
      'bundle' => 'sasg_partner',
      'locked' => '1',
    ),
    'sasg_partner_group' => array(
      'name' => 'Partner group',
      'bundle' => 'sasg_partner_group',
      'locked' => '1',
    ),
  );
  return $items;
}
