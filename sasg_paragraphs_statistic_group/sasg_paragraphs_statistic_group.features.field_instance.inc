<?php
/**
 * @file
 * sasg_paragraphs_statistic_group.features.field_instance.inc
 */

/**
 * Implements hook_field_default_field_instances().
 */
function sasg_paragraphs_statistic_group_field_default_field_instances() {
  $field_instances = array();

  // Exported field_instance:
  // 'paragraphs_item-sasg_statistic-field_sasg_statistic_description'.
  $field_instances['paragraphs_item-sasg_statistic-field_sasg_statistic_description'] = array(
    'bundle' => 'sasg_statistic',
    'default_value' => NULL,
    'deleted' => 0,
    'description' => '',
    'display' => array(
      'default' => array(
        'label' => 'hidden',
        'module' => 'text',
        'settings' => array(),
        'type' => 'text_default',
        'weight' => 2,
      ),
      'paragraphs_editor_preview' => array(
        'label' => 'above',
        'settings' => array(),
        'type' => 'hidden',
        'weight' => 0,
      ),
    ),
    'entity_type' => 'paragraphs_item',
    'fences_wrapper' => 'p',
    'field_name' => 'field_sasg_statistic_description',
    'label' => 'Statistic description',
    'required' => 1,
    'settings' => array(
      'text_processing' => 0,
      'user_register_form' => FALSE,
    ),
    'widget' => array(
      'active' => 1,
      'module' => 'text',
      'settings' => array(
        'size' => 60,
      ),
      'type' => 'text_textfield',
      'weight' => 2,
    ),
  );

  // Exported field_instance:
  // 'paragraphs_item-sasg_statistic-field_sasg_statistic_value'.
  $field_instances['paragraphs_item-sasg_statistic-field_sasg_statistic_value'] = array(
    'bundle' => 'sasg_statistic',
    'default_value' => NULL,
    'deleted' => 0,
    'description' => '',
    'display' => array(
      'default' => array(
        'label' => 'hidden',
        'module' => 'text',
        'settings' => array(),
        'type' => 'text_default',
        'weight' => 1,
      ),
      'paragraphs_editor_preview' => array(
        'label' => 'above',
        'settings' => array(),
        'type' => 'hidden',
        'weight' => 0,
      ),
    ),
    'entity_type' => 'paragraphs_item',
    'fences_wrapper' => 'span',
    'field_name' => 'field_sasg_statistic_value',
    'label' => 'Statistic value',
    'required' => 1,
    'settings' => array(
      'text_processing' => 0,
      'user_register_form' => FALSE,
    ),
    'widget' => array(
      'active' => 1,
      'module' => 'text',
      'settings' => array(
        'size' => 60,
      ),
      'type' => 'text_textfield',
      'weight' => 1,
    ),
  );

  // Exported field_instance:
  // 'paragraphs_item-sasg_statistic_group-field_sasg_statistics'.
  $field_instances['paragraphs_item-sasg_statistic_group-field_sasg_statistics'] = array(
    'bundle' => 'sasg_statistic_group',
    'default_value' => NULL,
    'deleted' => 0,
    'description' => '',
    'display' => array(
      'default' => array(
        'label' => 'hidden',
        'module' => 'paragraphs',
        'settings' => array(
          'view_mode' => 'full',
        ),
        'type' => 'paragraphs_view',
        'weight' => 1,
      ),
      'paragraphs_editor_preview' => array(
        'label' => 'above',
        'settings' => array(),
        'type' => 'hidden',
        'weight' => 0,
      ),
    ),
    'entity_type' => 'paragraphs_item',
    'fences_wrapper' => 'no_wrapper',
    'field_name' => 'field_sasg_statistics',
    'label' => 'Statistics',
    'required' => 0,
    'settings' => array(
      'add_mode' => 'select',
      'allowed_bundles' => array(
        'sasg_card' => -1,
        'sasg_card_deck' => -1,
        'sasg_carousel' => -1,
        'sasg_carousel_slide' => -1,
        'sasg_panel_group' => -1,
        'sasg_panel_item' => -1,
        'sasg_photo_grid' => -1,
        'sasg_photo_grid_item' => -1,
        'sasg_statistic' => 'sasg_statistic',
        'sasg_statistic_group' => -1,
        'sasg_tab_item' => -1,
        'sasg_tab_set' => -1,
        'sasg_table' => -1,
        'sasg_text_field' => -1,
        'sasg_video' => -1,
        'sasg_view' => -1,
      ),
      'bundle_weights' => array(
        'sasg_card' => -31,
        'sasg_card_deck' => -30,
        'sasg_carousel' => -29,
        'sasg_carousel_slide' => -28,
        'sasg_panel_group' => -27,
        'sasg_panel_item' => -26,
        'sasg_photo_grid' => -25,
        'sasg_photo_grid_item' => -24,
        'sasg_statistic' => -32,
        'sasg_statistic_group' => -23,
        'sasg_tab_item' => -21,
        'sasg_tab_set' => -20,
        'sasg_table' => -22,
        'sasg_text_field' => -19,
        'sasg_video' => -18,
        'sasg_view' => -17,
      ),
      'default_edit_mode' => 'open',
      'title' => 'Statistic',
      'title_multiple' => 'Statistics',
      'user_register_form' => FALSE,
    ),
    'widget' => array(
      'active' => 0,
      'module' => 'paragraphs',
      'settings' => array(),
      'type' => 'paragraphs_embed',
      'weight' => 1,
    ),
  );

  // Translatables
  // Included for use with string extractors like potx.
  t('Statistic description');
  t('Statistic value');
  t('Statistics');

  return $field_instances;
}
