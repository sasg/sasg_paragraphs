(function ($) {
  $(document).ready(function() {
    // stat carousel
    $('.stat-group-carousel').owlCarousel({
      loop:true,
      margin:24,
      nav:true,
      responsive:{
        0:{
          items:1
        },
        600:{
          items:2
        },
        1000:{
          items:3
        }
      }
    });
  });
}(jQuery));
