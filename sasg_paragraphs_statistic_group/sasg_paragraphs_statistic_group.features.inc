<?php
/**
 * @file
 * sasg_paragraphs_statistic_group.features.inc
 */

/**
 * Implements hook_ctools_plugin_api().
 */
function sasg_paragraphs_statistic_group_ctools_plugin_api($module = NULL, $api = NULL) {
  if ($module == "field_group" && $api == "field_group") {
    return array("version" => "1");
  }
}

/**
 * Implements hook_paragraphs_info().
 */
function sasg_paragraphs_statistic_group_paragraphs_info() {
  $items = array(
    'sasg_statistic' => array(
      'name' => 'Statistic',
      'bundle' => 'sasg_statistic',
      'locked' => '1',
    ),
    'sasg_statistic_group' => array(
      'name' => 'Statistic group',
      'bundle' => 'sasg_statistic_group',
      'locked' => '1',
    ),
  );
  return $items;
}
