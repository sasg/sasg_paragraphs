<?php
/**
 * @file
 * sasg_paragraphs_statistic_group.field_group.inc
 */

/**
 * Implements hook_field_group_info().
 */
function sasg_paragraphs_statistic_group_field_group_info() {
  $field_groups = array();

  $field_group = new stdClass();
  $field_group->disabled = FALSE; /* Edit this to true to make a default field_group disabled initially */
  $field_group->api_version = 1;
  $field_group->identifier = 'group_item|paragraphs_item|sasg_statistic|default';
  $field_group->group_name = 'group_item';
  $field_group->entity_type = 'paragraphs_item';
  $field_group->bundle = 'sasg_statistic';
  $field_group->mode = 'default';
  $field_group->parent_name = '';
  $field_group->data = array(
    'label' => 'Item',
    'weight' => '0',
    'children' => array(
      0 => 'field_sasg_statistic_value',
      1 => 'field_sasg_statistic_description',
    ),
    'format_type' => 'html-element',
    'format_settings' => array(
      'label' => 'Item',
      'instance_settings' => array(
        'id' => '',
        'classes' => 'item stat-item',
        'element' => 'div',
        'show_label' => '0',
        'label_element' => 'div',
        'attributes' => '',
      ),
    ),
  );
  $field_groups['group_item|paragraphs_item|sasg_statistic|default'] = $field_group;

  $field_group = new stdClass();
  $field_group->disabled = FALSE; /* Edit this to true to make a default field_group disabled initially */
  $field_group->api_version = 1;
  $field_group->identifier = 'group_tri_bg|paragraphs_item|sasg_statistic_group|default';
  $field_group->group_name = 'group_tri_bg';
  $field_group->entity_type = 'paragraphs_item';
  $field_group->bundle = 'sasg_statistic_group';
  $field_group->mode = 'default';
  $field_group->parent_name = '';
  $field_group->data = array(
    'label' => 'Triangle bg',
    'weight' => '0',
    'children' => array(
      0 => 'field_sasg_statistics',
    ),
    'format_type' => 'html-element',
    'format_settings' => array(
      'label' => 'Triangle bg',
      'instance_settings' => array(
        'id' => '',
        'classes' => 'bg-triangles-mosaic bg-sky bottom-buffer-30 stat-group-carousel owl-carousel',
        'element' => 'div',
        'show_label' => '0',
        'label_element' => 'div',
        'attributes' => '',
      ),
    ),
  );
  $field_groups['group_tri_bg|paragraphs_item|sasg_statistic_group|default'] = $field_group;

  // Translatables
  // Included for use with string extractors like potx.
  t('Item');
  t('Triangle bg');

  return $field_groups;
}
