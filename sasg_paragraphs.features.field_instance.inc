<?php
/**
 * @file
 * sasg_paragraphs.features.field_instance.inc
 */

/**
 * Implements hook_field_default_field_instances().
 */
function sasg_paragraphs_field_default_field_instances() {
  $field_instances = array();

  // Exported field_instance: 'paragraphs_item-sasg_card-field_sasg_card_links'.
  $field_instances['paragraphs_item-sasg_card-field_sasg_card_links'] = array(
    'bundle' => 'sasg_card',
    'default_value' => NULL,
    'deleted' => 0,
    'description' => '',
    'display' => array(
      'default' => array(
        'label' => 'hidden',
        'module' => 'link',
        'settings' => array(),
        'type' => 'link_default',
        'weight' => 12,
      ),
      'sasg_multiple_paragraphs' => array(
        'label' => 'hidden',
        'module' => 'link',
        'settings' => array(),
        'type' => 'link_default',
        'weight' => 14,
      ),
      'sasg_single_paragraph' => array(
        'label' => 'hidden',
        'module' => 'link',
        'settings' => array(),
        'type' => 'link_default',
        'weight' => 6,
      ),
    ),
    'entity_type' => 'paragraphs_item',
    'fences_wrapper' => 'no_wrapper',
    'field_name' => 'field_sasg_card_links',
    'label' => 'Links',
    'required' => 0,
    'settings' => array(
      'absolute_url' => 1,
      'attributes' => array(
        'class' => 'card-link',
        'configurable_class' => 1,
        'configurable_title' => 0,
        'rel' => '',
        'target' => 'default',
        'title' => '',
      ),
      'display' => array(
        'url_cutoff' => 80,
      ),
      'enable_tokens' => 1,
      'rel_remove' => 'default',
      'title' => 'optional',
      'title_label_use_field_label' => 0,
      'title_maxlength' => 128,
      'title_value' => '',
      'url' => 0,
      'user_register_form' => FALSE,
      'validate_url' => 1,
    ),
    'widget' => array(
      'active' => 0,
      'module' => 'link',
      'settings' => array(),
      'type' => 'link_field',
      'weight' => 4,
    ),
  );

  // Exported field_instance: 'paragraphs_item-sasg_card-field_sasg_card_photo'.
  $field_instances['paragraphs_item-sasg_card-field_sasg_card_photo'] = array(
    'bundle' => 'sasg_card',
    'deleted' => 0,
    'description' => '',
    'display' => array(
      'default' => array(
        'label' => 'hidden',
        'module' => 'image',
        'settings' => array(
          'image_link' => '',
          'image_style' => 'sasg_card_horizontal',
        ),
        'type' => 'image',
        'weight' => 9,
      ),
      'sasg_multiple_paragraphs' => array(
        'label' => 'hidden',
        'module' => 'image',
        'settings' => array(
          'image_link' => '',
          'image_style' => 'sasg_card',
        ),
        'type' => 'image',
        'weight' => 8,
      ),
      'sasg_single_paragraph' => array(
        'label' => 'hidden',
        'module' => 'image',
        'settings' => array(
          'image_link' => '',
          'image_style' => 'sasg_card',
        ),
        'type' => 'image',
        'weight' => 8,
      ),
    ),
    'entity_type' => 'paragraphs_item',
    'fences_wrapper' => 'no_wrapper',
    'field_name' => 'field_sasg_card_photo',
    'label' => 'Photo',
    'required' => 0,
    'settings' => array(
      'alt_field' => 1,
      'default_image' => 0,
      'file_directory' => '',
      'file_extensions' => 'png gif jpg jpeg',
      'max_filesize' => '',
      'max_resolution' => '',
      'min_resolution' => '',
      'title_field' => 0,
      'user_register_form' => FALSE,
    ),
    'widget' => array(
      'active' => 1,
      'module' => 'image',
      'settings' => array(
        'preview_image_style' => 'thumbnail',
        'progress_indicator' => 'throbber',
      ),
      'type' => 'image_image',
      'weight' => 2,
    ),
  );

  // Exported field_instance:
  // 'paragraphs_item-sasg_card-field_sasg_card_summary'.
  $field_instances['paragraphs_item-sasg_card-field_sasg_card_summary'] = array(
    'bundle' => 'sasg_card',
    'default_value' => NULL,
    'deleted' => 0,
    'description' => '',
    'display' => array(
      'default' => array(
        'label' => 'hidden',
        'module' => 'text',
        'settings' => array(),
        'type' => 'text_default',
        'weight' => 10,
      ),
      'sasg_multiple_paragraphs' => array(
        'label' => 'hidden',
        'module' => 'text',
        'settings' => array(),
        'type' => 'text_default',
        'weight' => 14,
      ),
      'sasg_single_paragraph' => array(
        'label' => 'hidden',
        'module' => 'text',
        'settings' => array(),
        'type' => 'text_default',
        'weight' => 3,
      ),
    ),
    'entity_type' => 'paragraphs_item',
    'fences_wrapper' => 'no_wrapper',
    'field_name' => 'field_sasg_card_summary',
    'label' => 'Summary',
    'required' => 0,
    'settings' => array(
      'text_processing' => 1,
      'user_register_form' => FALSE,
    ),
    'widget' => array(
      'active' => 1,
      'module' => 'text',
      'settings' => array(
        'rows' => 5,
      ),
      'type' => 'text_textarea',
      'weight' => 3,
    ),
  );

  // Exported field_instance: 'paragraphs_item-sasg_card-field_sasg_card_title'.
  $field_instances['paragraphs_item-sasg_card-field_sasg_card_title'] = array(
    'bundle' => 'sasg_card',
    'default_value' => NULL,
    'deleted' => 0,
    'description' => '',
    'display' => array(
      'default' => array(
        'label' => 'hidden',
        'module' => 'title',
        'settings' => array(
          'title_class' => 'margin-zero-top text-uppercase text-blue30w',
          'title_link' => '',
          'title_style' => 'h3',
        ),
        'type' => 'title_linked',
        'weight' => 9,
      ),
      'sasg_multiple_paragraphs' => array(
        'label' => 'hidden',
        'module' => 'title',
        'settings' => array(
          'title_class' => 'card-title',
          'title_link' => '',
          'title_style' => 'h4',
        ),
        'type' => 'title_linked',
        'weight' => 1,
      ),
      'sasg_single_paragraph' => array(
        'label' => 'hidden',
        'module' => 'title',
        'settings' => array(
          'title_class' => 'card-title',
          'title_link' => '',
          'title_style' => 'h4',
        ),
        'type' => 'title_linked',
        'weight' => 1,
      ),
    ),
    'entity_type' => 'paragraphs_item',
    'fences_wrapper' => 'no_wrapper',
    'field_name' => 'field_sasg_card_title',
    'label' => 'Title',
    'required' => 0,
    'settings' => array(
      'text_processing' => 0,
      'user_register_form' => FALSE,
    ),
    'widget' => array(
      'active' => 1,
      'module' => 'text',
      'settings' => array(
        'size' => 60,
      ),
      'type' => 'text_textfield',
      'weight' => 1,
    ),
  );

  // Exported field_instance: 'paragraphs_item-sasg_card_deck-field_sasg_cards'.
  $field_instances['paragraphs_item-sasg_card_deck-field_sasg_cards'] = array(
    'bundle' => 'sasg_card_deck',
    'default_value' => NULL,
    'deleted' => 0,
    'description' => '',
    'display' => array(
      'default' => array(
        'label' => 'hidden',
        'module' => 'paragraphs',
        'settings' => array(
          'view_mode' => 'sasg_multiple_paragraphs',
        ),
        'type' => 'paragraphs_view',
        'weight' => 1,
      ),
      'paragraphs_editor_preview' => array(
        'label' => 'above',
        'settings' => array(),
        'type' => 'hidden',
        'weight' => 0,
      ),
    ),
    'entity_type' => 'paragraphs_item',
    'fences_wrapper' => 'no_wrapper',
    'field_name' => 'field_sasg_cards',
    'label' => 'Cards',
    'required' => 1,
    'settings' => array(
      'add_mode' => 'select',
      'allowed_bundles' => array(
        'sasg_card' => 'sasg_card',
        'sasg_card_deck' => -1,
        'sasg_carousel' => -1,
        'sasg_carousel_slide' => -1,
        'sasg_panel_group' => -1,
        'sasg_panel_item' => -1,
        'sasg_photo_grid' => -1,
        'sasg_photo_grid_item' => -1,
        'sasg_tab_item' => -1,
        'sasg_tab_set' => -1,
        'sasg_text_field' => -1,
      ),
      'bundle_weights' => array(
        'sasg_card' => 2,
        'sasg_card_deck' => 3,
        'sasg_carousel' => 4,
        'sasg_carousel_slide' => 5,
        'sasg_panel_group' => 6,
        'sasg_panel_item' => 7,
        'sasg_photo_grid' => 8,
        'sasg_photo_grid_item' => 9,
        'sasg_tab_item' => 10,
        'sasg_tab_set' => 11,
        'sasg_text_field' => 12,
      ),
      'default_edit_mode' => 'open',
      'title' => 'Card',
      'title_multiple' => 'Cards',
      'user_register_form' => FALSE,
    ),
    'widget' => array(
      'active' => 0,
      'module' => 'paragraphs',
      'settings' => array(),
      'type' => 'paragraphs_embed',
      'weight' => 1,
    ),
  );

  // Exported field_instance:
  // 'paragraphs_item-sasg_carousel-field_sasg_carousel_slides'.
  $field_instances['paragraphs_item-sasg_carousel-field_sasg_carousel_slides'] = array(
    'bundle' => 'sasg_carousel',
    'default_value' => NULL,
    'deleted' => 0,
    'description' => '',
    'display' => array(
      'default' => array(
        'label' => 'hidden',
        'module' => 'paragraphs',
        'settings' => array(
          'view_mode' => 'full',
        ),
        'type' => 'paragraphs_view',
        'weight' => 2,
      ),
      'paragraphs_editor_preview' => array(
        'label' => 'above',
        'settings' => array(),
        'type' => 'hidden',
        'weight' => 0,
      ),
    ),
    'entity_type' => 'paragraphs_item',
    'fences_wrapper' => 'no_wrapper',
    'field_name' => 'field_sasg_carousel_slides',
    'label' => 'Carousel Slides',
    'required' => 1,
    'settings' => array(
      'add_mode' => 'select',
      'allowed_bundles' => array(
        'sasg_carousel' => -1,
        'sasg_carousel_slide' => 'sasg_carousel_slide',
      ),
      'bundle_weights' => array(
        'sasg_carousel' => 2,
        'sasg_carousel_slide' => 3,
      ),
      'default_edit_mode' => 'open',
      'title' => 'Carousel Slide',
      'title_multiple' => 'Carousel Slides',
      'user_register_form' => FALSE,
    ),
    'widget' => array(
      'active' => 0,
      'module' => 'paragraphs',
      'settings' => array(),
      'type' => 'paragraphs_embed',
      'weight' => 2,
    ),
  );

  // Exported field_instance:
  // 'paragraphs_item-sasg_carousel_slide-field_sasg_carousel_caption'.
  $field_instances['paragraphs_item-sasg_carousel_slide-field_sasg_carousel_caption'] = array(
    'bundle' => 'sasg_carousel_slide',
    'default_value' => NULL,
    'deleted' => 0,
    'description' => '',
    'display' => array(
      'default' => array(
        'label' => 'hidden',
        'module' => 'text',
        'settings' => array(),
        'type' => 'text_default',
        'weight' => 3,
      ),
      'paragraphs_editor_preview' => array(
        'label' => 'above',
        'settings' => array(),
        'type' => 'hidden',
        'weight' => 0,
      ),
    ),
    'entity_type' => 'paragraphs_item',
    'fences_wrapper' => 'no_wrapper',
    'field_name' => 'field_sasg_carousel_caption',
    'label' => 'Caption',
    'required' => 0,
    'settings' => array(
      'text_processing' => 0,
      'user_register_form' => FALSE,
    ),
    'widget' => array(
      'active' => 1,
      'module' => 'text',
      'settings' => array(
        'size' => 60,
      ),
      'type' => 'text_textfield',
      'weight' => 3,
    ),
  );

  // Exported field_instance:
  // 'paragraphs_item-sasg_carousel_slide-field_sasg_carousel_photo'.
  $field_instances['paragraphs_item-sasg_carousel_slide-field_sasg_carousel_photo'] = array(
    'bundle' => 'sasg_carousel_slide',
    'deleted' => 0,
    'description' => '',
    'display' => array(
      'default' => array(
        'label' => 'hidden',
        'module' => 'image',
        'settings' => array(
          'image_link' => '',
          'image_style' => 'sasg_carousel',
        ),
        'type' => 'image',
        'weight' => 0,
      ),
      'paragraphs_editor_preview' => array(
        'label' => 'above',
        'settings' => array(),
        'type' => 'hidden',
        'weight' => 0,
      ),
    ),
    'entity_type' => 'paragraphs_item',
    'fences_wrapper' => 'no_wrapper',
    'field_name' => 'field_sasg_carousel_photo',
    'label' => 'Photo',
    'required' => 1,
    'settings' => array(
      'alt_field' => 1,
      'default_image' => 0,
      'file_directory' => '',
      'file_extensions' => 'png gif jpg jpeg',
      'max_filesize' => '',
      'max_resolution' => '',
      'min_resolution' => '945x200',
      'title_field' => 1,
      'user_register_form' => FALSE,
    ),
    'widget' => array(
      'active' => 1,
      'module' => 'image',
      'settings' => array(
        'preview_image_style' => 'thumbnail',
        'progress_indicator' => 'throbber',
      ),
      'type' => 'image_image',
      'weight' => 1,
    ),
  );

  // Exported field_instance:
  // 'paragraphs_item-sasg_panel_group-field_sasg_panel_items'.
  $field_instances['paragraphs_item-sasg_panel_group-field_sasg_panel_items'] = array(
    'bundle' => 'sasg_panel_group',
    'default_value' => NULL,
    'deleted' => 0,
    'description' => '',
    'display' => array(
      'default' => array(
        'label' => 'hidden',
        'module' => 'paragraphs',
        'settings' => array(
          'view_mode' => 'full',
        ),
        'type' => 'paragraphs_view',
        'weight' => 1,
      ),
      'paragraphs_editor_preview' => array(
        'label' => 'above',
        'settings' => array(),
        'type' => 'hidden',
        'weight' => 0,
      ),
    ),
    'entity_type' => 'paragraphs_item',
    'fences_wrapper' => 'no_wrapper',
    'field_name' => 'field_sasg_panel_items',
    'label' => 'Panels',
    'required' => 1,
    'settings' => array(
      'add_mode' => 'select',
      'allowed_bundles' => array(
        'sasg_card' => -1,
        'sasg_card_deck' => -1,
        'sasg_carousel' => -1,
        'sasg_carousel_slide' => -1,
        'sasg_panel_group' => -1,
        'sasg_panel_item' => 'sasg_panel_item',
        'sasg_partner' => -1,
        'sasg_partner_group' => -1,
        'sasg_photo_grid' => -1,
        'sasg_photo_grid_item' => -1,
        'sasg_statistic' => -1,
        'sasg_statistic_group' => -1,
        'sasg_tab_item' => -1,
        'sasg_tab_set' => -1,
        'sasg_table' => -1,
        'sasg_text_field' => -1,
        'sasg_video' => -1,
        'sasg_view' => -1,
      ),
      'bundle_weights' => array(
        'sasg_card' => 19,
        'sasg_card_deck' => 20,
        'sasg_carousel' => 2,
        'sasg_carousel_slide' => 3,
        'sasg_panel_group' => 6,
        'sasg_panel_item' => 7,
        'sasg_partner' => 25,
        'sasg_partner_group' => 26,
        'sasg_photo_grid' => 16,
        'sasg_photo_grid_item' => 17,
        'sasg_statistic' => 29,
        'sasg_statistic_group' => 30,
        'sasg_tab_item' => 8,
        'sasg_tab_set' => 9,
        'sasg_table' => 31,
        'sasg_text_field' => 10,
        'sasg_video' => 35,
        'sasg_view' => 36,
      ),
      'default_edit_mode' => 'open',
      'title' => 'Panel',
      'title_multiple' => 'Panels',
      'user_register_form' => FALSE,
    ),
    'widget' => array(
      'active' => 0,
      'module' => 'paragraphs',
      'settings' => array(),
      'type' => 'paragraphs_embed',
      'weight' => 1,
    ),
  );

  // Exported field_instance:
  // 'paragraphs_item-sasg_panel_item-field_sasg_panel_body'.
  $field_instances['paragraphs_item-sasg_panel_item-field_sasg_panel_body'] = array(
    'bundle' => 'sasg_panel_item',
    'default_value' => NULL,
    'deleted' => 0,
    'description' => '',
    'display' => array(
      'default' => array(
        'label' => 'hidden',
        'module' => 'paragraphs',
        'settings' => array(
          'view_mode' => 'full',
        ),
        'type' => 'paragraphs_view',
        'weight' => 2,
      ),
      'paragraphs_editor_preview' => array(
        'label' => 'above',
        'settings' => array(),
        'type' => 'hidden',
        'weight' => 0,
      ),
    ),
    'entity_type' => 'paragraphs_item',
    'fences_wrapper' => 'no_wrapper',
    'field_name' => 'field_sasg_panel_body',
    'label' => 'Panel body',
    'required' => 1,
    'settings' => array(
      'add_mode' => 'select',
      'allowed_bundles' => array(
        'sasg_card' => 'sasg_card',
        'sasg_card_deck' => -1,
        'sasg_carousel' => -1,
        'sasg_carousel_slide' => -1,
        'sasg_panel_group' => -1,
        'sasg_panel_item' => -1,
        'sasg_photo_grid' => -1,
        'sasg_photo_grid_item' => -1,
        'sasg_tab_item' => -1,
        'sasg_tab_set' => -1,
        'sasg_table' => 'sasg_table',
        'sasg_text_field' => 'sasg_text_field',
        'sasg_video' => -1,
        'sasg_view' => -1,
      ),
      'bundle_weights' => array(
        'sasg_card' => -26,
        'sasg_card_deck' => -23,
        'sasg_carousel' => -24,
        'sasg_carousel_slide' => -22,
        'sasg_panel_group' => -21,
        'sasg_panel_item' => -20,
        'sasg_photo_grid' => -25,
        'sasg_photo_grid_item' => -19,
        'sasg_tab_item' => -18,
        'sasg_tab_set' => -17,
        'sasg_table' => -27,
        'sasg_text_field' => -28,
        'sasg_video' => -16,
        'sasg_view' => -15,
      ),
      'default_edit_mode' => 'open',
      'title' => 'Panel Content Item',
      'title_multiple' => 'Panel Content Items',
      'user_register_form' => FALSE,
    ),
    'widget' => array(
      'active' => 0,
      'module' => 'paragraphs',
      'settings' => array(),
      'type' => 'paragraphs_embed',
      'weight' => 2,
    ),
  );

  // Exported field_instance:
  // 'paragraphs_item-sasg_panel_item-field_sasg_panel_heading'.
  $field_instances['paragraphs_item-sasg_panel_item-field_sasg_panel_heading'] = array(
    'bundle' => 'sasg_panel_item',
    'default_value' => NULL,
    'deleted' => 0,
    'description' => '',
    'display' => array(
      'default' => array(
        'label' => 'hidden',
        'module' => 'text',
        'settings' => array(),
        'type' => 'text_default',
        'weight' => 1,
      ),
      'paragraphs_editor_preview' => array(
        'label' => 'above',
        'settings' => array(),
        'type' => 'hidden',
        'weight' => 0,
      ),
    ),
    'entity_type' => 'paragraphs_item',
    'fences_wrapper' => 'no_wrapper',
    'field_name' => 'field_sasg_panel_heading',
    'label' => 'Panel heading',
    'required' => 1,
    'settings' => array(
      'text_processing' => 0,
      'user_register_form' => FALSE,
    ),
    'widget' => array(
      'active' => 1,
      'module' => 'text',
      'settings' => array(
        'size' => 60,
      ),
      'type' => 'text_textfield',
      'weight' => 1,
    ),
  );

  // Exported field_instance:
  // 'paragraphs_item-sasg_tab_item-field_sasg_tab_content'.
  $field_instances['paragraphs_item-sasg_tab_item-field_sasg_tab_content'] = array(
    'bundle' => 'sasg_tab_item',
    'default_value' => NULL,
    'deleted' => 0,
    'description' => '',
    'display' => array(
      'default' => array(
        'label' => 'hidden',
        'module' => 'paragraphs',
        'settings' => array(
          'view_mode' => 'full',
        ),
        'type' => 'paragraphs_view',
        'weight' => 1,
      ),
      'paragraphs_editor_preview' => array(
        'label' => 'above',
        'settings' => array(),
        'type' => 'hidden',
        'weight' => 0,
      ),
    ),
    'entity_type' => 'paragraphs_item',
    'fences_wrapper' => 'no_wrapper',
    'field_name' => 'field_sasg_tab_content',
    'label' => 'Tab Content',
    'required' => 1,
    'settings' => array(
      'add_mode' => 'select',
      'allowed_bundles' => array(
        'sasg_card' => 'sasg_card',
        'sasg_card_deck' => -1,
        'sasg_carousel' => -1,
        'sasg_carousel_slide' => -1,
        'sasg_panel_group' => -1,
        'sasg_panel_item' => -1,
        'sasg_photo_grid' => -1,
        'sasg_photo_grid_item' => -1,
        'sasg_tab_item' => -1,
        'sasg_tab_set' => -1,
        'sasg_table' => 'sasg_table',
        'sasg_text_field' => 'sasg_text_field',
        'sasg_video' => -1,
        'sasg_view' => -1,
      ),
      'bundle_weights' => array(
        'sasg_card' => -26,
        'sasg_card_deck' => -22,
        'sasg_carousel' => -24,
        'sasg_carousel_slide' => -21,
        'sasg_panel_group' => -25,
        'sasg_panel_item' => -18,
        'sasg_photo_grid' => -23,
        'sasg_photo_grid_item' => -17,
        'sasg_tab_item' => -20,
        'sasg_tab_set' => -19,
        'sasg_table' => -27,
        'sasg_text_field' => -28,
        'sasg_video' => -16,
        'sasg_view' => -15,
      ),
      'default_edit_mode' => 'open',
      'title' => 'Tab Content Item',
      'title_multiple' => 'Tab Content Items',
      'user_register_form' => FALSE,
    ),
    'widget' => array(
      'active' => 0,
      'module' => 'paragraphs',
      'settings' => array(),
      'type' => 'paragraphs_embed',
      'weight' => 2,
    ),
  );

  // Exported field_instance:
  // 'paragraphs_item-sasg_tab_item-field_sasg_tab_title'.
  $field_instances['paragraphs_item-sasg_tab_item-field_sasg_tab_title'] = array(
    'bundle' => 'sasg_tab_item',
    'default_value' => NULL,
    'deleted' => 0,
    'description' => '',
    'display' => array(
      'default' => array(
        'label' => 'hidden',
        'module' => 'text',
        'settings' => array(),
        'type' => 'text_default',
        'weight' => 0,
      ),
      'paragraphs_editor_preview' => array(
        'label' => 'above',
        'settings' => array(),
        'type' => 'hidden',
        'weight' => 0,
      ),
    ),
    'entity_type' => 'paragraphs_item',
    'fences_wrapper' => 'no_wrapper',
    'field_name' => 'field_sasg_tab_title',
    'label' => 'Tab Name',
    'required' => 1,
    'settings' => array(
      'text_processing' => 0,
      'user_register_form' => FALSE,
    ),
    'widget' => array(
      'active' => 1,
      'module' => 'text',
      'settings' => array(
        'size' => 60,
      ),
      'type' => 'text_textfield',
      'weight' => 1,
    ),
  );

  // Exported field_instance:
  // 'paragraphs_item-sasg_tab_set-field_sasg_tab_sections'.
  $field_instances['paragraphs_item-sasg_tab_set-field_sasg_tab_sections'] = array(
    'bundle' => 'sasg_tab_set',
    'default_value' => NULL,
    'deleted' => 0,
    'description' => '',
    'display' => array(
      'default' => array(
        'label' => 'hidden',
        'module' => 'paragraphs',
        'settings' => array(
          'view_mode' => 'full',
        ),
        'type' => 'paragraphs_view',
        'weight' => 0,
      ),
      'paragraphs_editor_preview' => array(
        'label' => 'above',
        'settings' => array(),
        'type' => 'hidden',
        'weight' => 0,
      ),
    ),
    'entity_type' => 'paragraphs_item',
    'fences_wrapper' => 'no_wrapper',
    'field_name' => 'field_sasg_tab_sections',
    'label' => 'Tab Sections',
    'required' => 1,
    'settings' => array(
      'add_mode' => 'select',
      'allowed_bundles' => array(
        'sasg_carousel' => -1,
        'sasg_carousel_slide' => -1,
        'sasg_panel_group' => -1,
        'sasg_panel_item' => -1,
        'sasg_photo_grid' => -1,
        'sasg_photo_grid_item' => -1,
        'sasg_tab_item' => 'sasg_tab_item',
        'sasg_tab_set' => -1,
        'sasg_text_field' => -1,
      ),
      'bundle_weights' => array(
        'sasg_carousel' => 4,
        'sasg_carousel_slide' => 5,
        'sasg_panel_group' => 14,
        'sasg_panel_item' => 15,
        'sasg_photo_grid' => 16,
        'sasg_photo_grid_item' => 17,
        'sasg_tab_item' => 8,
        'sasg_tab_set' => 9,
        'sasg_text_field' => 10,
      ),
      'default_edit_mode' => 'open',
      'title' => 'Tab',
      'title_multiple' => 'Tabs',
      'user_register_form' => FALSE,
    ),
    'widget' => array(
      'active' => 0,
      'module' => 'paragraphs',
      'settings' => array(),
      'type' => 'paragraphs_embed',
      'weight' => 1,
    ),
  );

  // Exported field_instance: 'paragraphs_item-sasg_table-field_sasg_table'.
  $field_instances['paragraphs_item-sasg_table-field_sasg_table'] = array(
    'bundle' => 'sasg_table',
    'default_value' => array(
      0 => array(
        'tablefield' => array(
          'caption' => '',
          'import' => array(
            'file' => '',
            'import' => 'Upload CSV',
          ),
          'paste' => array(
            'data' => '',
            'paste_delimiter' => '',
            'paste_import' => 'Import & Rebuild',
          ),
          'rebuild' => array(
            'count_cols' => 5,
            'count_rows' => 5,
            'rebuild' => 'Rebuild Table',
          ),
          'tabledata' => array(
            'row_0' => array(
              'col_0' => '',
              'col_1' => '',
              'col_2' => '',
              'col_3' => '',
              'col_4' => '',
              'weight' => 1,
            ),
            'row_1' => array(
              'col_0' => '',
              'col_1' => '',
              'col_2' => '',
              'col_3' => '',
              'col_4' => '',
              'weight' => 2,
            ),
            'row_2' => array(
              'col_0' => '',
              'col_1' => '',
              'col_2' => '',
              'col_3' => '',
              'col_4' => '',
              'weight' => 3,
            ),
            'row_3' => array(
              'col_0' => '',
              'col_1' => '',
              'col_2' => '',
              'col_3' => '',
              'col_4' => '',
              'weight' => 4,
            ),
            'row_4' => array(
              'col_0' => '',
              'col_1' => '',
              'col_2' => '',
              'col_3' => '',
              'col_4' => '',
              'weight' => 5,
            ),
          ),
        ),
      ),
    ),
    'deleted' => 0,
    'description' => '',
    'display' => array(
      'default' => array(
        'label' => 'hidden',
        'module' => 'tablefield',
        'settings' => array(
          'export_csv' => 0,
          'header_orientation' => 'Horizontal',
          'hide_cols_skip_head' => 0,
          'hide_empty_cols' => 0,
          'hide_empty_rows' => 0,
          'hide_header' => 0,
          'sortable' => FALSE,
          'sticky_header' => 0,
          'striping' => 1,
          'trim_trailing_cols' => 0,
          'trim_trailing_rows' => 0,
        ),
        'type' => 'tablefield_default',
        'weight' => 0,
      ),
      'paragraphs_editor_preview' => array(
        'label' => 'above',
        'settings' => array(),
        'type' => 'hidden',
        'weight' => 0,
      ),
    ),
    'entity_type' => 'paragraphs_item',
    'fences_wrapper' => 'no_wrapper',
    'field_name' => 'field_sasg_table',
    'label' => 'Table',
    'required' => 0,
    'settings' => array(
      'user_register_form' => FALSE,
    ),
    'widget' => array(
      'active' => 1,
      'module' => 'tablefield',
      'settings' => array(
        'cell_processing' => 0,
        'data_sources' => array(
          'paste' => 'paste',
          'upload' => 'upload',
        ),
        'input_type' => 'textfield',
        'lock_values' => 0,
        'max_length' => 2048,
        'restrict_rebuild' => 0,
      ),
      'type' => 'tablefield',
      'weight' => 1,
    ),
  );

  // Exported field_instance: 'paragraphs_item-sasg_text_field-field_sasg_text'.
  $field_instances['paragraphs_item-sasg_text_field-field_sasg_text'] = array(
    'bundle' => 'sasg_text_field',
    'default_value' => NULL,
    'deleted' => 0,
    'description' => '',
    'display' => array(
      'default' => array(
        'label' => 'hidden',
        'module' => 'text',
        'settings' => array(),
        'type' => 'text_default',
        'weight' => 0,
      ),
      'paragraphs_editor_preview' => array(
        'label' => 'above',
        'settings' => array(),
        'type' => 'hidden',
        'weight' => 0,
      ),
    ),
    'entity_type' => 'paragraphs_item',
    'fences_wrapper' => 'no_wrapper',
    'field_name' => 'field_sasg_text',
    'label' => 'Text',
    'required' => 1,
    'settings' => array(
      'text_processing' => 1,
      'user_register_form' => FALSE,
    ),
    'widget' => array(
      'active' => 1,
      'module' => 'text',
      'settings' => array(
        'rows' => 5,
      ),
      'type' => 'text_textarea',
      'weight' => 1,
    ),
  );

  // Translatables
  // Included for use with string extractors like potx.
  t('Caption');
  t('Cards');
  t('Carousel Slides');
  t('Links');
  t('Panel body');
  t('Panel heading');
  t('Panels');
  t('Photo');
  t('Summary');
  t('Tab Content');
  t('Tab Name');
  t('Tab Sections');
  t('Table');
  t('Text');
  t('Title');

  return $field_instances;
}
