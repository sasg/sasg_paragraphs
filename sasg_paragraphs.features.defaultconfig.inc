<?php
/**
 * @file
 * sasg_paragraphs.features.defaultconfig.inc
 */

/**
 * Implements hook_defaultconfig_features().
 */
function sasg_paragraphs_defaultconfig_features() {
  return array(
    'sasg_paragraphs' => array(
      'field_default_fields' => 'field_default_fields',
      'field_group_info' => 'field_group_info',
      'strongarm' => 'strongarm',
    ),
  );
}

/**
 * Implements hook_defaultconfig_field_default_fields().
 */
function sasg_paragraphs_defaultconfig_field_default_fields() {
  $fields = array();

  // Exported field: 'paragraphs_item-sasg_carousel-field_sasg_carousel_slides'.
  $fields['paragraphs_item-sasg_carousel-field_sasg_carousel_slides'] = array(
    'field_config' => array(
      'active' => 1,
      'cardinality' => -1,
      'deleted' => 0,
      'entity_types' => array(),
      'field_name' => 'field_sasg_carousel_slides',
      'foreign keys' => array(),
      'indexes' => array(),
      'locked' => 0,
      'module' => 'paragraphs',
      'settings' => array(),
      'translatable' => 0,
      'type' => 'paragraphs',
    ),
    'field_instance' => array(
      'bundle' => 'sasg_carousel',
      'default_value' => NULL,
      'deleted' => 0,
      'description' => '',
      'display' => array(
        'default' => array(
          'label' => 'hidden',
          'module' => 'paragraphs',
          'settings' => array(
            'view_mode' => 'full',
          ),
          'type' => 'paragraphs_view',
          'weight' => 2,
        ),
        'paragraphs_editor_preview' => array(
          'label' => 'above',
          'settings' => array(),
          'type' => 'hidden',
          'weight' => 0,
        ),
      ),
      'entity_type' => 'paragraphs_item',
      'fences_wrapper' => 'no_wrapper',
      'field_name' => 'field_sasg_carousel_slides',
      'label' => 'Carousel Slides',
      'required' => 1,
      'settings' => array(
        'add_mode' => 'select',
        'allowed_bundles' => array(
          'sasg_carousel' => -1,
          'sasg_carousel_slide' => 'sasg_carousel_slide',
        ),
        'bundle_weights' => array(
          'sasg_carousel' => 2,
          'sasg_carousel_slide' => 3,
        ),
        'default_edit_mode' => 'open',
        'title' => 'Carousel Slide',
        'title_multiple' => 'Carousel Slides',
        'user_register_form' => FALSE,
      ),
      'widget' => array(
        'active' => 0,
        'module' => 'paragraphs',
        'settings' => array(),
        'type' => 'paragraphs_embed',
        'weight' => 2,
      ),
    ),
  );

  // Exported field: 'paragraphs_item-sasg_carousel_slide-field_sasg_carousel_caption'.
  $fields['paragraphs_item-sasg_carousel_slide-field_sasg_carousel_caption'] = array(
    'field_config' => array(
      'active' => 1,
      'cardinality' => 1,
      'deleted' => 0,
      'entity_types' => array(),
      'field_name' => 'field_sasg_carousel_caption',
      'foreign keys' => array(
        'format' => array(
          'columns' => array(
            'format' => 'format',
          ),
          'table' => 'filter_format',
        ),
      ),
      'indexes' => array(
        'format' => array(
          0 => 'format',
        ),
      ),
      'locked' => 0,
      'module' => 'text',
      'settings' => array(
        'max_length' => 255,
      ),
      'translatable' => 0,
      'type' => 'text',
    ),
    'field_instance' => array(
      'bundle' => 'sasg_carousel_slide',
      'default_value' => NULL,
      'deleted' => 0,
      'description' => '',
      'display' => array(
        'default' => array(
          'label' => 'hidden',
          'module' => 'text',
          'settings' => array(),
          'type' => 'text_default',
          'weight' => 3,
        ),
        'paragraphs_editor_preview' => array(
          'label' => 'above',
          'settings' => array(),
          'type' => 'hidden',
          'weight' => 0,
        ),
      ),
      'entity_type' => 'paragraphs_item',
      'fences_wrapper' => 'no_wrapper',
      'field_name' => 'field_sasg_carousel_caption',
      'label' => 'Caption',
      'required' => 0,
      'settings' => array(
        'text_processing' => 0,
        'user_register_form' => FALSE,
      ),
      'widget' => array(
        'active' => 1,
        'module' => 'text',
        'settings' => array(
          'size' => 60,
        ),
        'type' => 'text_textfield',
        'weight' => 3,
      ),
    ),
  );

  // Exported field: 'paragraphs_item-sasg_carousel_slide-field_sasg_carousel_photo'.
  $fields['paragraphs_item-sasg_carousel_slide-field_sasg_carousel_photo'] = array(
    'field_config' => array(
      'active' => 1,
      'cardinality' => 1,
      'deleted' => 0,
      'entity_types' => array(),
      'field_name' => 'field_sasg_carousel_photo',
      'foreign keys' => array(
        'fid' => array(
          'columns' => array(
            'fid' => 'fid',
          ),
          'table' => 'file_managed',
        ),
      ),
      'indexes' => array(
        'fid' => array(
          0 => 'fid',
        ),
      ),
      'locked' => 0,
      'module' => 'image',
      'settings' => array(
        'default_image' => 0,
        'uri_scheme' => 'public',
      ),
      'translatable' => 0,
      'type' => 'image',
    ),
    'field_instance' => array(
      'bundle' => 'sasg_carousel_slide',
      'deleted' => 0,
      'description' => '',
      'display' => array(
        'default' => array(
          'label' => 'hidden',
          'module' => 'image',
          'settings' => array(
            'image_link' => '',
            'image_style' => '',
          ),
          'type' => 'image',
          'weight' => 0,
        ),
        'paragraphs_editor_preview' => array(
          'label' => 'above',
          'settings' => array(),
          'type' => 'hidden',
          'weight' => 0,
        ),
      ),
      'entity_type' => 'paragraphs_item',
      'fences_wrapper' => 'no_wrapper',
      'field_name' => 'field_sasg_carousel_photo',
      'label' => 'Photo',
      'required' => 1,
      'settings' => array(
        'alt_field' => 1,
        'default_image' => 0,
        'file_directory' => '',
        'file_extensions' => 'png gif jpg jpeg',
        'max_filesize' => '',
        'max_resolution' => '',
        'min_resolution' => '945x200',
        'title_field' => 1,
        'user_register_form' => FALSE,
      ),
      'widget' => array(
        'active' => 1,
        'module' => 'image',
        'settings' => array(
          'preview_image_style' => 'thumbnail',
          'progress_indicator' => 'throbber',
        ),
        'type' => 'image_image',
        'weight' => 1,
      ),
    ),
  );

  // Exported field: 'paragraphs_item-sasg_panel_group-field_sasg_panel_items'.
  $fields['paragraphs_item-sasg_panel_group-field_sasg_panel_items'] = array(
    'field_config' => array(
      'active' => 1,
      'cardinality' => -1,
      'deleted' => 0,
      'entity_types' => array(),
      'field_name' => 'field_sasg_panel_items',
      'foreign keys' => array(),
      'indexes' => array(),
      'locked' => 0,
      'module' => 'paragraphs',
      'settings' => array(),
      'translatable' => 0,
      'type' => 'paragraphs',
    ),
    'field_instance' => array(
      'bundle' => 'sasg_panel_group',
      'default_value' => NULL,
      'deleted' => 0,
      'description' => '',
      'display' => array(
        'default' => array(
          'label' => 'hidden',
          'module' => 'paragraphs',
          'settings' => array(
            'view_mode' => 'full',
          ),
          'type' => 'paragraphs_view',
          'weight' => 1,
        ),
        'paragraphs_editor_preview' => array(
          'label' => 'above',
          'settings' => array(),
          'type' => 'hidden',
          'weight' => 0,
        ),
      ),
      'entity_type' => 'paragraphs_item',
      'fences_wrapper' => 'no_wrapper',
      'field_name' => 'field_sasg_panel_items',
      'label' => 'Panels',
      'required' => 0,
      'settings' => array(
        'add_mode' => 'select',
        'allowed_bundles' => array(
          'sasg_carousel' => -1,
          'sasg_carousel_slide' => -1,
          'sasg_gallery' => -1,
          'sasg_gallery_item' => -1,
          'sasg_panel_group' => -1,
          'sasg_panel_item' => 'sasg_panel_item',
          'sasg_tab_item' => -1,
          'sasg_tab_set' => -1,
          'sasg_text_field' => -1,
        ),
        'bundle_weights' => array(
          'sasg_carousel' => 2,
          'sasg_carousel_slide' => 3,
          'sasg_gallery' => 4,
          'sasg_gallery_item' => 5,
          'sasg_panel_group' => 6,
          'sasg_panel_item' => 7,
          'sasg_tab_item' => 8,
          'sasg_tab_set' => 9,
          'sasg_text_field' => 10,
        ),
        'default_edit_mode' => 'open',
        'title' => 'Panel',
        'title_multiple' => 'Panels',
        'user_register_form' => FALSE,
      ),
      'widget' => array(
        'active' => 0,
        'module' => 'paragraphs',
        'settings' => array(),
        'type' => 'paragraphs_embed',
        'weight' => 1,
      ),
    ),
  );

  // Exported field: 'paragraphs_item-sasg_panel_item-field_sasg_panel_body'.
  $fields['paragraphs_item-sasg_panel_item-field_sasg_panel_body'] = array(
    'field_config' => array(
      'active' => 1,
      'cardinality' => 1,
      'deleted' => 0,
      'entity_types' => array(),
      'field_name' => 'field_sasg_panel_body',
      'foreign keys' => array(
        'format' => array(
          'columns' => array(
            'format' => 'format',
          ),
          'table' => 'filter_format',
        ),
      ),
      'indexes' => array(
        'format' => array(
          0 => 'format',
        ),
      ),
      'locked' => 0,
      'module' => 'text',
      'settings' => array(),
      'translatable' => 0,
      'type' => 'text_long',
    ),
    'field_instance' => array(
      'bundle' => 'sasg_panel_item',
      'default_value' => NULL,
      'deleted' => 0,
      'description' => '',
      'display' => array(
        'default' => array(
          'label' => 'hidden',
          'module' => 'text',
          'settings' => array(),
          'type' => 'text_default',
          'weight' => 2,
        ),
        'paragraphs_editor_preview' => array(
          'label' => 'above',
          'settings' => array(),
          'type' => 'hidden',
          'weight' => 0,
        ),
      ),
      'entity_type' => 'paragraphs_item',
      'fences_wrapper' => 'no_wrapper',
      'field_name' => 'field_sasg_panel_body',
      'label' => 'Panel body',
      'required' => 0,
      'settings' => array(
        'text_processing' => 0,
        'user_register_form' => FALSE,
      ),
      'widget' => array(
        'active' => 1,
        'module' => 'text',
        'settings' => array(
          'rows' => 5,
        ),
        'type' => 'text_textarea',
        'weight' => 2,
      ),
    ),
  );

  // Exported field: 'paragraphs_item-sasg_panel_item-field_sasg_panel_heading'.
  $fields['paragraphs_item-sasg_panel_item-field_sasg_panel_heading'] = array(
    'field_config' => array(
      'active' => 1,
      'cardinality' => 1,
      'deleted' => 0,
      'entity_types' => array(),
      'field_name' => 'field_sasg_panel_heading',
      'foreign keys' => array(
        'format' => array(
          'columns' => array(
            'format' => 'format',
          ),
          'table' => 'filter_format',
        ),
      ),
      'indexes' => array(
        'format' => array(
          0 => 'format',
        ),
      ),
      'locked' => 0,
      'module' => 'text',
      'settings' => array(
        'max_length' => 255,
      ),
      'translatable' => 0,
      'type' => 'text',
    ),
    'field_instance' => array(
      'bundle' => 'sasg_panel_item',
      'default_value' => NULL,
      'deleted' => 0,
      'description' => '',
      'display' => array(
        'default' => array(
          'label' => 'hidden',
          'module' => 'text',
          'settings' => array(),
          'type' => 'text_default',
          'weight' => 1,
        ),
        'paragraphs_editor_preview' => array(
          'label' => 'above',
          'settings' => array(),
          'type' => 'hidden',
          'weight' => 0,
        ),
      ),
      'entity_type' => 'paragraphs_item',
      'fences_wrapper' => 'no_wrapper',
      'field_name' => 'field_sasg_panel_heading',
      'label' => 'Panel heading',
      'required' => 1,
      'settings' => array(
        'text_processing' => 0,
        'user_register_form' => FALSE,
      ),
      'widget' => array(
        'active' => 1,
        'module' => 'text',
        'settings' => array(
          'size' => 60,
        ),
        'type' => 'text_textfield',
        'weight' => 1,
      ),
    ),
  );

  // Exported field: 'paragraphs_item-sasg_photo_grid-field_sasg_photo_grid_items'.
  $fields['paragraphs_item-sasg_photo_grid-field_sasg_photo_grid_items'] = array(
    'field_config' => array(
      'active' => 1,
      'cardinality' => -1,
      'deleted' => 0,
      'entity_types' => array(),
      'field_name' => 'field_sasg_photo_grid_items',
      'foreign keys' => array(),
      'indexes' => array(),
      'locked' => 0,
      'module' => 'paragraphs',
      'settings' => array(),
      'translatable' => 0,
      'type' => 'paragraphs',
    ),
    'field_instance' => array(
      'bundle' => 'sasg_photo_grid',
      'default_value' => NULL,
      'deleted' => 0,
      'description' => '',
      'display' => array(
        'default' => array(
          'label' => 'hidden',
          'module' => 'paragraphs',
          'settings' => array(
            'view_mode' => 'full',
          ),
          'type' => 'paragraphs_view',
          'weight' => 0,
        ),
        'paragraphs_editor_preview' => array(
          'label' => 'above',
          'settings' => array(),
          'type' => 'hidden',
          'weight' => 0,
        ),
      ),
      'entity_type' => 'paragraphs_item',
      'fences_wrapper' => 'no_wrapper',
      'field_name' => 'field_sasg_photo_grid_items',
      'label' => 'Photos',
      'required' => 0,
      'settings' => array(
        'add_mode' => 'select',
        'allowed_bundles' => array(
          'sasg_carousel' => -1,
          'sasg_carousel_slide' => -1,
          'sasg_panel_group' => -1,
          'sasg_panel_item' => -1,
          'sasg_photo_grid' => -1,
          'sasg_photo_grid_item' => 'sasg_photo_grid_item',
          'sasg_tab_item' => -1,
          'sasg_tab_set' => -1,
          'sasg_text_field' => -1,
        ),
        'bundle_weights' => array(
          'sasg_carousel' => 2,
          'sasg_carousel_slide' => 3,
          'sasg_panel_group' => 4,
          'sasg_panel_item' => 5,
          'sasg_photo_grid' => 6,
          'sasg_photo_grid_item' => 7,
          'sasg_tab_item' => 8,
          'sasg_tab_set' => 9,
          'sasg_text_field' => 10,
        ),
        'default_edit_mode' => 'open',
        'title' => 'Photo',
        'title_multiple' => 'Photos',
        'user_register_form' => FALSE,
      ),
      'widget' => array(
        'active' => 0,
        'module' => 'paragraphs',
        'settings' => array(),
        'type' => 'paragraphs_embed',
        'weight' => 1,
      ),
    ),
  );

  // Exported field: 'paragraphs_item-sasg_photo_grid_item-field_sasg_grid_caption'.
  $fields['paragraphs_item-sasg_photo_grid_item-field_sasg_grid_caption'] = array(
    'field_config' => array(
      'active' => 1,
      'cardinality' => 1,
      'deleted' => 0,
      'entity_types' => array(),
      'field_name' => 'field_sasg_grid_caption',
      'foreign keys' => array(
        'format' => array(
          'columns' => array(
            'format' => 'format',
          ),
          'table' => 'filter_format',
        ),
      ),
      'indexes' => array(
        'format' => array(
          0 => 'format',
        ),
      ),
      'locked' => 0,
      'module' => 'text',
      'settings' => array(
        'max_length' => 255,
      ),
      'translatable' => 0,
      'type' => 'text',
    ),
    'field_instance' => array(
      'bundle' => 'sasg_photo_grid_item',
      'default_value' => NULL,
      'deleted' => 0,
      'description' => '',
      'display' => array(
        'default' => array(
          'label' => 'hidden',
          'module' => 'text',
          'settings' => array(),
          'type' => 'text_default',
          'weight' => 1,
        ),
        'paragraphs_editor_preview' => array(
          'label' => 'above',
          'settings' => array(),
          'type' => 'hidden',
          'weight' => 0,
        ),
      ),
      'entity_type' => 'paragraphs_item',
      'fences_wrapper' => 'no_wrapper',
      'field_name' => 'field_sasg_grid_caption',
      'label' => 'Caption',
      'required' => 0,
      'settings' => array(
        'text_processing' => 0,
        'user_register_form' => FALSE,
      ),
      'widget' => array(
        'active' => 1,
        'module' => 'text',
        'settings' => array(
          'size' => 60,
        ),
        'type' => 'text_textfield',
        'weight' => 2,
      ),
    ),
  );

  // Exported field: 'paragraphs_item-sasg_photo_grid_item-field_sasg_grid_photo'.
  $fields['paragraphs_item-sasg_photo_grid_item-field_sasg_grid_photo'] = array(
    'field_config' => array(
      'active' => 1,
      'cardinality' => 1,
      'deleted' => 0,
      'entity_types' => array(),
      'field_name' => 'field_sasg_grid_photo',
      'foreign keys' => array(
        'fid' => array(
          'columns' => array(
            'fid' => 'fid',
          ),
          'table' => 'file_managed',
        ),
      ),
      'indexes' => array(
        'fid' => array(
          0 => 'fid',
        ),
      ),
      'locked' => 0,
      'module' => 'image',
      'settings' => array(
        'default_image' => 0,
        'uri_scheme' => 'public',
      ),
      'translatable' => 0,
      'type' => 'image',
    ),
    'field_instance' => array(
      'bundle' => 'sasg_photo_grid_item',
      'deleted' => 0,
      'description' => '',
      'display' => array(
        'default' => array(
          'label' => 'hidden',
          'module' => 'image_formatter_link_to_image_style',
          'settings' => array(
            'image_class' => '',
            'image_link_style' => 'sasg_photo_grid_large',
            'image_style' => 'sasg_photo_grid_thumb',
            'link_class' => '',
            'link_rel' => '',
          ),
          'type' => 'image_formatter_link_to_image_style',
          'weight' => 1,
        ),
        'paragraphs_editor_preview' => array(
          'label' => 'above',
          'settings' => array(),
          'type' => 'hidden',
          'weight' => 0,
        ),
      ),
      'entity_type' => 'paragraphs_item',
      'fences_wrapper' => 'no_wrapper',
      'field_name' => 'field_sasg_grid_photo',
      'label' => 'Photo',
      'required' => 1,
      'settings' => array(
        'alt_field' => 1,
        'default_image' => 0,
        'file_directory' => '',
        'file_extensions' => 'png gif jpg jpeg',
        'max_filesize' => '',
        'max_resolution' => '',
        'min_resolution' => '',
        'title_field' => 0,
        'user_register_form' => FALSE,
      ),
      'widget' => array(
        'active' => 1,
        'module' => 'image',
        'settings' => array(
          'preview_image_style' => 'thumbnail',
          'progress_indicator' => 'throbber',
        ),
        'type' => 'image_image',
        'weight' => 1,
      ),
    ),
  );

  // Exported field: 'paragraphs_item-sasg_tab_item-field_sasg_tab_content'.
  $fields['paragraphs_item-sasg_tab_item-field_sasg_tab_content'] = array(
    'field_config' => array(
      'active' => 1,
      'cardinality' => -1,
      'deleted' => 0,
      'entity_types' => array(),
      'field_name' => 'field_sasg_tab_content',
      'foreign keys' => array(),
      'indexes' => array(),
      'locked' => 0,
      'module' => 'paragraphs',
      'settings' => array(),
      'translatable' => 0,
      'type' => 'paragraphs',
    ),
    'field_instance' => array(
      'bundle' => 'sasg_tab_item',
      'default_value' => NULL,
      'deleted' => 0,
      'description' => '',
      'display' => array(
        'default' => array(
          'label' => 'hidden',
          'module' => 'paragraphs',
          'settings' => array(
            'view_mode' => 'full',
          ),
          'type' => 'paragraphs_view',
          'weight' => 1,
        ),
        'paragraphs_editor_preview' => array(
          'label' => 'above',
          'settings' => array(),
          'type' => 'hidden',
          'weight' => 0,
        ),
      ),
      'entity_type' => 'paragraphs_item',
      'fences_wrapper' => 'div_div_div',
      'field_name' => 'field_sasg_tab_content',
      'label' => 'Tab Content',
      'required' => 0,
      'settings' => array(
        'add_mode' => 'select',
        'allowed_bundles' => array(
          'sasg_carousel' => -1,
          'sasg_carousel_slide' => -1,
          'sasg_panel_group' => -1,
          'sasg_panel_item' => -1,
          'sasg_photo_grid' => -1,
          'sasg_photo_grid_item' => -1,
          'sasg_tab_item' => -1,
          'sasg_tab_set' => -1,
          'sasg_text_field' => 'sasg_text_field',
        ),
        'bundle_weights' => array(
          'sasg_carousel' => 4,
          'sasg_carousel_slide' => 5,
          'sasg_panel_group' => 14,
          'sasg_panel_item' => 15,
          'sasg_photo_grid' => 16,
          'sasg_photo_grid_item' => 17,
          'sasg_tab_item' => 8,
          'sasg_tab_set' => 9,
          'sasg_text_field' => 10,
        ),
        'default_edit_mode' => 'open',
        'title' => 'Tab Content Item',
        'title_multiple' => 'Tab Content Items',
        'user_register_form' => FALSE,
      ),
      'widget' => array(
        'active' => 0,
        'module' => 'paragraphs',
        'settings' => array(),
        'type' => 'paragraphs_embed',
        'weight' => 2,
      ),
    ),
  );

  // Exported field: 'paragraphs_item-sasg_tab_item-field_sasg_tab_title'.
  $fields['paragraphs_item-sasg_tab_item-field_sasg_tab_title'] = array(
    'field_config' => array(
      'active' => 1,
      'cardinality' => 1,
      'deleted' => 0,
      'entity_types' => array(),
      'field_name' => 'field_sasg_tab_title',
      'foreign keys' => array(
        'format' => array(
          'columns' => array(
            'format' => 'format',
          ),
          'table' => 'filter_format',
        ),
      ),
      'indexes' => array(
        'format' => array(
          0 => 'format',
        ),
      ),
      'locked' => 0,
      'module' => 'text',
      'settings' => array(
        'max_length' => 255,
      ),
      'translatable' => 0,
      'type' => 'text',
    ),
    'field_instance' => array(
      'bundle' => 'sasg_tab_item',
      'default_value' => NULL,
      'deleted' => 0,
      'description' => '',
      'display' => array(
        'default' => array(
          'label' => 'hidden',
          'module' => 'text',
          'settings' => array(),
          'type' => 'text_default',
          'weight' => 0,
        ),
        'paragraphs_editor_preview' => array(
          'label' => 'above',
          'settings' => array(),
          'type' => 'hidden',
          'weight' => 0,
        ),
      ),
      'entity_type' => 'paragraphs_item',
      'fences_wrapper' => 'no_wrapper',
      'field_name' => 'field_sasg_tab_title',
      'label' => 'Tab Name',
      'required' => 1,
      'settings' => array(
        'text_processing' => 0,
        'user_register_form' => FALSE,
      ),
      'widget' => array(
        'active' => 1,
        'module' => 'text',
        'settings' => array(
          'size' => 60,
        ),
        'type' => 'text_textfield',
        'weight' => 1,
      ),
    ),
  );

  // Exported field: 'paragraphs_item-sasg_tab_set-field_sasg_tab_sections'.
  $fields['paragraphs_item-sasg_tab_set-field_sasg_tab_sections'] = array(
    'field_config' => array(
      'active' => 1,
      'cardinality' => -1,
      'deleted' => 0,
      'entity_types' => array(),
      'field_name' => 'field_sasg_tab_sections',
      'foreign keys' => array(),
      'indexes' => array(),
      'locked' => 0,
      'module' => 'paragraphs',
      'settings' => array(),
      'translatable' => 0,
      'type' => 'paragraphs',
    ),
    'field_instance' => array(
      'bundle' => 'sasg_tab_set',
      'default_value' => NULL,
      'deleted' => 0,
      'description' => '',
      'display' => array(
        'default' => array(
          'label' => 'hidden',
          'module' => 'paragraphs',
          'settings' => array(
            'view_mode' => 'full',
          ),
          'type' => 'paragraphs_view',
          'weight' => 0,
        ),
        'paragraphs_editor_preview' => array(
          'label' => 'above',
          'settings' => array(),
          'type' => 'hidden',
          'weight' => 0,
        ),
      ),
      'entity_type' => 'paragraphs_item',
      'fences_wrapper' => 'no_wrapper',
      'field_name' => 'field_sasg_tab_sections',
      'label' => 'Tab Sections',
      'required' => 0,
      'settings' => array(
        'add_mode' => 'select',
        'allowed_bundles' => array(
          'sasg_carousel' => -1,
          'sasg_carousel_slide' => -1,
          'sasg_panel_group' => -1,
          'sasg_panel_item' => -1,
          'sasg_photo_grid' => -1,
          'sasg_photo_grid_item' => -1,
          'sasg_tab_item' => 'sasg_tab_item',
          'sasg_tab_set' => -1,
          'sasg_text_field' => -1,
        ),
        'bundle_weights' => array(
          'sasg_carousel' => 4,
          'sasg_carousel_slide' => 5,
          'sasg_panel_group' => 14,
          'sasg_panel_item' => 15,
          'sasg_photo_grid' => 16,
          'sasg_photo_grid_item' => 17,
          'sasg_tab_item' => 8,
          'sasg_tab_set' => 9,
          'sasg_text_field' => 10,
        ),
        'default_edit_mode' => 'open',
        'title' => 'Tab',
        'title_multiple' => 'Tabs',
        'user_register_form' => FALSE,
      ),
      'widget' => array(
        'active' => 0,
        'module' => 'paragraphs',
        'settings' => array(),
        'type' => 'paragraphs_embed',
        'weight' => 1,
      ),
    ),
  );

  // Exported field: 'paragraphs_item-sasg_text_field-field_sasg_text'.
  $fields['paragraphs_item-sasg_text_field-field_sasg_text'] = array(
    'field_config' => array(
      'active' => 1,
      'cardinality' => 1,
      'deleted' => 0,
      'entity_types' => array(),
      'field_name' => 'field_sasg_text',
      'foreign keys' => array(
        'format' => array(
          'columns' => array(
            'format' => 'format',
          ),
          'table' => 'filter_format',
        ),
      ),
      'indexes' => array(
        'format' => array(
          0 => 'format',
        ),
      ),
      'locked' => 0,
      'module' => 'text',
      'settings' => array(),
      'translatable' => 0,
      'type' => 'text_long',
    ),
    'field_instance' => array(
      'bundle' => 'sasg_text_field',
      'default_value' => NULL,
      'deleted' => 0,
      'description' => '',
      'display' => array(
        'default' => array(
          'label' => 'hidden',
          'module' => 'text',
          'settings' => array(),
          'type' => 'text_default',
          'weight' => 0,
        ),
        'paragraphs_editor_preview' => array(
          'label' => 'above',
          'settings' => array(),
          'type' => 'hidden',
          'weight' => 0,
        ),
      ),
      'entity_type' => 'paragraphs_item',
      'fences_wrapper' => 'no_wrapper',
      'field_name' => 'field_sasg_text',
      'label' => 'Text',
      'required' => 0,
      'settings' => array(
        'text_processing' => 0,
        'user_register_form' => FALSE,
      ),
      'widget' => array(
        'active' => 1,
        'module' => 'text',
        'settings' => array(
          'rows' => 5,
        ),
        'type' => 'text_textarea',
        'weight' => 1,
      ),
    ),
  );

  // Translatables
  // Included for use with string extractors like potx.
  t('Caption');
  t('Carousel Slides');
  t('Panel body');
  t('Panel heading');
  t('Panels');
  t('Photo');
  t('Photos');
  t('Tab Content');
  t('Tab Name');
  t('Tab Sections');
  t('Text');

  return $fields;
}

/**
 * Implements hook_defaultconfig_field_group_info().
 */
function sasg_paragraphs_defaultconfig_field_group_info() {
  $field_groups = array();

  $field_group = new stdClass();
  $field_group->disabled = FALSE; /* Edit this to true to make a default field_group disabled initially */
  $field_group->api_version = 1;
  $field_group->identifier = 'group_caption|paragraphs_item|sasg_photo_grid_item|default';
  $field_group->group_name = 'group_caption';
  $field_group->entity_type = 'paragraphs_item';
  $field_group->bundle = 'sasg_photo_grid_item';
  $field_group->mode = 'default';
  $field_group->parent_name = 'group_photo_grid_item';
  $field_group->data = array(
    'label' => 'Caption',
    'weight' => '2',
    'children' => array(
      0 => 'field_sasg_grid_caption',
    ),
    'format_type' => 'html-element',
    'format_settings' => array(
      'label' => 'Caption',
      'instance_settings' => array(
        'id' => '',
        'classes' => 'photo-grid-caption text-muted',
        'element' => 'figcaption',
        'show_label' => '0',
        'label_element' => 'div',
        'attributes' => '',
      ),
    ),
  );
  $field_groups['group_caption|paragraphs_item|sasg_photo_grid_item|default'] = $field_group;

  $field_group = new stdClass();
  $field_group->disabled = FALSE; /* Edit this to true to make a default field_group disabled initially */
  $field_group->api_version = 1;
  $field_group->identifier = 'group_panel_group|paragraphs_item|sasg_panel_group|default';
  $field_group->group_name = 'group_panel_group';
  $field_group->entity_type = 'paragraphs_item';
  $field_group->bundle = 'sasg_panel_group';
  $field_group->mode = 'default';
  $field_group->parent_name = '';
  $field_group->data = array(
    'label' => 'Panel group',
    'weight' => '0',
    'children' => array(
      0 => 'field_sasg_panel_items',
    ),
    'format_type' => 'html-element',
    'format_settings' => array(
      'label' => 'Panel group',
      'instance_settings' => array(
        'id' => '',
        'classes' => 'panel-group',
        'element' => 'div',
        'show_label' => '0',
        'label_element' => 'div',
        'attributes' => 'role="tablist" aria-multiselectable="true"',
      ),
    ),
  );
  $field_groups['group_panel_group|paragraphs_item|sasg_panel_group|default'] = $field_group;

  $field_group = new stdClass();
  $field_group->disabled = FALSE; /* Edit this to true to make a default field_group disabled initially */
  $field_group->api_version = 1;
  $field_group->identifier = 'group_panel|paragraphs_item|sasg_panel_item|default';
  $field_group->group_name = 'group_panel';
  $field_group->entity_type = 'paragraphs_item';
  $field_group->bundle = 'sasg_panel_item';
  $field_group->mode = 'default';
  $field_group->parent_name = '';
  $field_group->data = array(
    'label' => 'Panel',
    'weight' => '0',
    'children' => array(
      0 => 'field_sasg_panel_heading',
      1 => 'field_sasg_panel_body',
    ),
    'format_type' => 'html-element',
    'format_settings' => array(
      'label' => 'Panel',
      'instance_settings' => array(
        'id' => '',
        'classes' => 'panel panel-default',
        'element' => 'div',
        'show_label' => '0',
        'label_element' => 'div',
        'attributes' => '',
      ),
    ),
  );
  $field_groups['group_panel|paragraphs_item|sasg_panel_item|default'] = $field_group;

  $field_group = new stdClass();
  $field_group->disabled = FALSE; /* Edit this to true to make a default field_group disabled initially */
  $field_group->api_version = 1;
  $field_group->identifier = 'group_photo_grid_item|paragraphs_item|sasg_photo_grid_item|default';
  $field_group->group_name = 'group_photo_grid_item';
  $field_group->entity_type = 'paragraphs_item';
  $field_group->bundle = 'sasg_photo_grid_item';
  $field_group->mode = 'default';
  $field_group->parent_name = '';
  $field_group->data = array(
    'label' => 'Photo grid item',
    'weight' => '0',
    'children' => array(
      0 => 'field_sasg_grid_photo',
      1 => 'group_caption',
    ),
    'format_type' => 'html-element',
    'format_settings' => array(
      'label' => 'Photo grid item',
      'instance_settings' => array(
        'id' => '',
        'classes' => 'photo-grid-item col-sm-6 col-md-4 col-lg-3',
        'element' => 'div',
        'show_label' => '0',
        'label_element' => 'div',
        'attributes' => '',
      ),
    ),
  );
  $field_groups['group_photo_grid_item|paragraphs_item|sasg_photo_grid_item|default'] = $field_group;

  $field_group = new stdClass();
  $field_group->disabled = FALSE; /* Edit this to true to make a default field_group disabled initially */
  $field_group->api_version = 1;
  $field_group->identifier = 'group_photo_grid|paragraphs_item|sasg_photo_grid|default';
  $field_group->group_name = 'group_photo_grid';
  $field_group->entity_type = 'paragraphs_item';
  $field_group->bundle = 'sasg_photo_grid';
  $field_group->mode = 'default';
  $field_group->parent_name = '';
  $field_group->data = array(
    'label' => 'Photo grid',
    'weight' => '3',
    'children' => array(
      0 => 'field_sasg_photo_grid_items',
    ),
    'format_type' => 'html-element',
    'format_settings' => array(
      'label' => 'Photo grid',
      'instance_settings' => array(
        'id' => '',
        'classes' => 'photo-grid row',
        'element' => 'div',
        'show_label' => '0',
        'label_element' => 'div',
        'attributes' => '',
      ),
    ),
  );
  $field_groups['group_photo_grid|paragraphs_item|sasg_photo_grid|default'] = $field_group;

  // Translatables
  // Included for use with string extractors like potx.
  t('Caption');
  t('Panel');
  t('Panel group');
  t('Photo grid');
  t('Photo grid item');

  return $field_groups;
}

/**
 * Implements hook_defaultconfig_strongarm().
 */
function sasg_paragraphs_defaultconfig_strongarm() {
  $export = array();

  $strongarm = new stdClass();
  $strongarm->disabled = FALSE; /* Edit this to true to make a default strongarm disabled initially */
  $strongarm->api_version = 1;
  $strongarm->name = 'field_bundle_settings_paragraphs_item__sasg_panel_group';
  $strongarm->value = array(
    'view_modes' => array(
      'paragraphs_editor_preview' => array(
        'custom_settings' => TRUE,
      ),
      'full' => array(
        'custom_settings' => FALSE,
      ),
    ),
    'extra_fields' => array(
      'form' => array(),
      'display' => array(),
    ),
  );
  $export['field_bundle_settings_paragraphs_item__sasg_panel_group'] = $strongarm;

  $strongarm = new stdClass();
  $strongarm->disabled = FALSE; /* Edit this to true to make a default strongarm disabled initially */
  $strongarm->api_version = 1;
  $strongarm->name = 'field_bundle_settings_paragraphs_item__sasg_panel_item';
  $strongarm->value = array(
    'view_modes' => array(
      'paragraphs_editor_preview' => array(
        'custom_settings' => TRUE,
      ),
      'full' => array(
        'custom_settings' => FALSE,
      ),
    ),
    'extra_fields' => array(
      'form' => array(),
      'display' => array(),
    ),
  );
  $export['field_bundle_settings_paragraphs_item__sasg_panel_item'] = $strongarm;

  $strongarm = new stdClass();
  $strongarm->disabled = FALSE; /* Edit this to true to make a default strongarm disabled initially */
  $strongarm->api_version = 1;
  $strongarm->name = 'field_bundle_settings_paragraphs_item__sasg_photo_grid';
  $strongarm->value = array(
    'view_modes' => array(
      'paragraphs_editor_preview' => array(
        'custom_settings' => TRUE,
      ),
      'full' => array(
        'custom_settings' => FALSE,
      ),
    ),
    'extra_fields' => array(
      'form' => array(),
      'display' => array(),
    ),
  );
  $export['field_bundle_settings_paragraphs_item__sasg_photo_grid'] = $strongarm;

  $strongarm = new stdClass();
  $strongarm->disabled = FALSE; /* Edit this to true to make a default strongarm disabled initially */
  $strongarm->api_version = 1;
  $strongarm->name = 'field_bundle_settings_paragraphs_item__sasg_photo_grid_item';
  $strongarm->value = array(
    'view_modes' => array(
      'paragraphs_editor_preview' => array(
        'custom_settings' => TRUE,
      ),
      'full' => array(
        'custom_settings' => FALSE,
      ),
    ),
    'extra_fields' => array(
      'form' => array(),
      'display' => array(),
    ),
  );
  $export['field_bundle_settings_paragraphs_item__sasg_photo_grid_item'] = $strongarm;

  $strongarm = new stdClass();
  $strongarm->disabled = FALSE; /* Edit this to true to make a default strongarm disabled initially */
  $strongarm->api_version = 1;
  $strongarm->name = 'field_bundle_settings_paragraphs_item__sasg_tab_item';
  $strongarm->value = array(
    'view_modes' => array(
      'paragraphs_editor_preview' => array(
        'custom_settings' => TRUE,
      ),
      'full' => array(
        'custom_settings' => FALSE,
      ),
    ),
    'extra_fields' => array(
      'form' => array(),
      'display' => array(),
    ),
  );
  $export['field_bundle_settings_paragraphs_item__sasg_tab_item'] = $strongarm;

  $strongarm = new stdClass();
  $strongarm->disabled = FALSE; /* Edit this to true to make a default strongarm disabled initially */
  $strongarm->api_version = 1;
  $strongarm->name = 'field_bundle_settings_paragraphs_item__sasg_tab_set';
  $strongarm->value = array(
    'view_modes' => array(
      'paragraphs_editor_preview' => array(
        'custom_settings' => TRUE,
      ),
      'full' => array(
        'custom_settings' => FALSE,
      ),
    ),
    'extra_fields' => array(
      'form' => array(),
      'display' => array(),
    ),
  );
  $export['field_bundle_settings_paragraphs_item__sasg_tab_set'] = $strongarm;

  $strongarm = new stdClass();
  $strongarm->disabled = FALSE; /* Edit this to true to make a default strongarm disabled initially */
  $strongarm->api_version = 1;
  $strongarm->name = 'field_bundle_settings_paragraphs_item__sasg_text_field';
  $strongarm->value = array(
    'view_modes' => array(
      'paragraphs_editor_preview' => array(
        'custom_settings' => TRUE,
      ),
      'full' => array(
        'custom_settings' => FALSE,
      ),
    ),
    'extra_fields' => array(
      'form' => array(),
      'display' => array(),
    ),
  );
  $export['field_bundle_settings_paragraphs_item__sasg_text_field'] = $strongarm;

  return $export;
}
