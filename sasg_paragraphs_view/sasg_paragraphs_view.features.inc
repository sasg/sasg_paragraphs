<?php
/**
 * @file
 * sasg_paragraphs_view.features.inc
 */

/**
 * Implements hook_paragraphs_info().
 */
function sasg_paragraphs_view_paragraphs_info() {
  $items = array(
    'sasg_view' => array(
      'name' => 'View',
      'bundle' => 'sasg_view',
      'locked' => '1',
    ),
  );
  return $items;
}
