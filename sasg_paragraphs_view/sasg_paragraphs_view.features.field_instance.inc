<?php
/**
 * @file
 * sasg_paragraphs_view.features.field_instance.inc
 */

/**
 * Implements hook_field_default_field_instances().
 */
function sasg_paragraphs_view_field_default_field_instances() {
  $field_instances = array();

  // Exported field_instance: 'paragraphs_item-sasg_view-field_sasg_view'.
  $field_instances['paragraphs_item-sasg_view-field_sasg_view'] = array(
    'bundle' => 'sasg_view',
    'default_value' => NULL,
    'deleted' => 0,
    'description' => '',
    'display' => array(
      'default' => array(
        'label' => 'hidden',
        'module' => 'viewfield',
        'settings' => array(),
        'type' => 'viewfield_default',
        'weight' => 0,
      ),
      'paragraphs_editor_preview' => array(
        'label' => 'above',
        'settings' => array(),
        'type' => 'hidden',
        'weight' => 0,
      ),
    ),
    'entity_type' => 'paragraphs_item',
    'fences_wrapper' => 'no_wrapper',
    'field_name' => 'field_sasg_view',
    'label' => 'View',
    'required' => 0,
    'settings' => array(
      'allowed_views' => array(
        'ua_person_directory' => 0,
      ),
      'force_default' => 0,
      'user_register_form' => FALSE,
    ),
    'widget' => array(
      'active' => 0,
      'module' => 'viewfield',
      'settings' => array(),
      'type' => 'viewfield_select',
      'weight' => 1,
    ),
  );

  // Translatables
  // Included for use with string extractors like potx.
  t('View');

  return $field_instances;
}
