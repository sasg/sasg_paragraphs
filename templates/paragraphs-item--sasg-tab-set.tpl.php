<?php

/**
 * @file
 * Default theme implementation for a single paragraph item.
 *
 * Available variables:
 * - $content: An array of content items. Use render($content) to print them
 *   all, or print a subset such as render($content['field_example']). Use
 *   hide($content['field_example']) to temporarily suppress the printing of a
 *   given element.
 * - $classes: String of classes that can be used to style contextually through
 *   CSS. It can be manipulated through the variable $classes_array from
 *   preprocess functions. By default the following classes are available, where
 *   the parts enclosed by {} are replaced by the appropriate values:
 *   - entity
 *   - entity-paragraphs-item
 *   - paragraphs-item-{bundle}
 *
 * Other variables:
 * - $classes_array: Array of html class attribute values. It is flattened into
 *   a string within the variable $classes.
 *
 * @see template_preprocess()
 * @see template_preprocess_entity()
 * @see template_process()
 */
?>
<div class="sasg-tabs bottom-buffer-30">
  <ul class="nav nav-tabs" role="tablist">
    <?php foreach ($content['field_sasg_tab_sections'] as $key=>$array): ?>
      <?php if (is_int($key)): ?>
        <?php $class = ($key == 0) ? 'tab-label active' : 'tab-label'; ?>
        <?php foreach ($array['entity']['paragraphs_item'] as $key2=>$item): ?>
          <?php if (is_int($key2)): ?>
            <li class="<?php print $class; ?>" role="presentation">
              <a href="#tab-<?php print $id ."-". $key; ?>" aria-controls="tab-<?php print $id ."-". $key; ?>" role="tab" data-toggle="tab"><?php print render($item['field_sasg_tab_title']); ?></a>
            </li>
          <?php endif; ?>
        <?php endforeach; ?>
      <?php endif; ?>
    <?php endforeach; ?>
  </ul>
  <div class="tab-content">
    <?php foreach ($content['field_sasg_tab_sections'] as $key=>$array): ?>
      <?php if (is_int($key)): ?>
        <?php $class = ($key == 0) ? 'tab-pane active' : 'tab-pane'; ?>
        <?php foreach ($array['entity']['paragraphs_item'] as $key2=>$item): ?>
          <?php if (is_int($key2)): ?>
            <div role="tabpanel" class="<?php print $class; ?>" id="tab-<?php print $id ."-". $key; ?>">
              <?php print render($item['field_sasg_tab_content']); ?>
            </div>
          <?php endif; ?>
        <?php endforeach; ?>
      <?php endif; ?>
    <?php endforeach; ?>
  </div>
</div>