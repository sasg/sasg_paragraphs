<?php
/**
 * @file
 * sasg_paragraphs_photo_grid.field_group.inc
 */

/**
 * Implements hook_field_group_info().
 */
function sasg_paragraphs_photo_grid_field_group_info() {
  $field_groups = array();

  $field_group = new stdClass();
  $field_group->disabled = FALSE; /* Edit this to true to make a default field_group disabled initially */
  $field_group->api_version = 1;
  $field_group->identifier = 'group_caption|paragraphs_item|sasg_photo_grid_item|default';
  $field_group->group_name = 'group_caption';
  $field_group->entity_type = 'paragraphs_item';
  $field_group->bundle = 'sasg_photo_grid_item';
  $field_group->mode = 'default';
  $field_group->parent_name = 'group_photo_single';
  $field_group->data = array(
    'label' => 'Caption',
    'weight' => '2',
    'children' => array(
      0 => 'field_sasg_grid_caption',
    ),
    'format_type' => 'html-element',
    'format_settings' => array(
      'label' => 'Caption',
      'instance_settings' => array(
        'id' => '',
        'classes' => 'photo-grid-caption text-blue60b bg-cool-gray',
        'element' => 'figcaption',
        'show_label' => '0',
        'label_element' => 'div',
        'attributes' => '',
      ),
    ),
  );
  $field_groups['group_caption|paragraphs_item|sasg_photo_grid_item|default'] = $field_group;

  $field_group = new stdClass();
  $field_group->disabled = FALSE; /* Edit this to true to make a default field_group disabled initially */
  $field_group->api_version = 1;
  $field_group->identifier = 'group_caption|paragraphs_item|sasg_photo_grid_item|sasg_multiple_paragraphs';
  $field_group->group_name = 'group_caption';
  $field_group->entity_type = 'paragraphs_item';
  $field_group->bundle = 'sasg_photo_grid_item';
  $field_group->mode = 'sasg_multiple_paragraphs';
  $field_group->parent_name = 'group_photo_grid_item_content';
  $field_group->data = array(
    'label' => 'Caption',
    'weight' => '3',
    'children' => array(
      0 => 'field_sasg_grid_caption',
    ),
    'format_type' => 'html-element',
    'format_settings' => array(
      'label' => 'Caption',
      'instance_settings' => array(
        'id' => '',
        'classes' => 'photo-grid-caption text-blue60b bg-cool-gray',
        'element' => 'div',
        'show_label' => '0',
        'label_element' => 'div',
        'attributes' => '',
      ),
    ),
  );
  $field_groups['group_caption|paragraphs_item|sasg_photo_grid_item|sasg_multiple_paragraphs'] = $field_group;

  $field_group = new stdClass();
  $field_group->disabled = FALSE; /* Edit this to true to make a default field_group disabled initially */
  $field_group->api_version = 1;
  $field_group->identifier = 'group_photo_grid_item_content|paragraphs_item|sasg_photo_grid_item|sasg_multiple_paragraphs';
  $field_group->group_name = 'group_photo_grid_item_content';
  $field_group->entity_type = 'paragraphs_item';
  $field_group->bundle = 'sasg_photo_grid_item';
  $field_group->mode = 'sasg_multiple_paragraphs';
  $field_group->parent_name = 'group_photo_grid_item';
  $field_group->data = array(
    'label' => 'Photo grid item content',
    'weight' => '4',
    'children' => array(
      0 => 'field_sasg_grid_photo',
      1 => 'group_caption',
    ),
    'format_type' => 'html-element',
    'format_settings' => array(
      'label' => 'Photo grid item content',
      'instance_settings' => array(
        'id' => '',
        'classes' => 'photo-grid-item-content',
        'element' => 'div',
        'show_label' => '0',
        'label_element' => 'div',
        'attributes' => '',
      ),
    ),
  );
  $field_groups['group_photo_grid_item_content|paragraphs_item|sasg_photo_grid_item|sasg_multiple_paragraphs'] = $field_group;

  $field_group = new stdClass();
  $field_group->disabled = FALSE; /* Edit this to true to make a default field_group disabled initially */
  $field_group->api_version = 1;
  $field_group->identifier = 'group_photo_grid_item|paragraphs_item|sasg_photo_grid_item|sasg_multiple_paragraphs';
  $field_group->group_name = 'group_photo_grid_item';
  $field_group->entity_type = 'paragraphs_item';
  $field_group->bundle = 'sasg_photo_grid_item';
  $field_group->mode = 'sasg_multiple_paragraphs';
  $field_group->parent_name = '';
  $field_group->data = array(
    'label' => 'Photo grid item',
    'weight' => '0',
    'children' => array(
      0 => 'group_photo_grid_item_content',
    ),
    'format_type' => 'html-element',
    'format_settings' => array(
      'label' => 'Photo grid item',
      'instance_settings' => array(
        'id' => '',
        'classes' => 'photo-grid-item col-sm-6 col-md-4',
        'element' => 'div',
        'show_label' => '0',
        'label_element' => 'div',
        'attributes' => '',
      ),
    ),
  );
  $field_groups['group_photo_grid_item|paragraphs_item|sasg_photo_grid_item|sasg_multiple_paragraphs'] = $field_group;

  $field_group = new stdClass();
  $field_group->disabled = FALSE; /* Edit this to true to make a default field_group disabled initially */
  $field_group->api_version = 1;
  $field_group->identifier = 'group_photo_grid|paragraphs_item|sasg_photo_grid|default';
  $field_group->group_name = 'group_photo_grid';
  $field_group->entity_type = 'paragraphs_item';
  $field_group->bundle = 'sasg_photo_grid';
  $field_group->mode = 'default';
  $field_group->parent_name = '';
  $field_group->data = array(
    'label' => 'Photo grid',
    'weight' => '3',
    'children' => array(
      0 => 'field_sasg_photo_grid_items',
    ),
    'format_type' => 'html-element',
    'format_settings' => array(
      'label' => 'Photo grid',
      'instance_settings' => array(
        'id' => '',
        'classes' => 'photo-grid row no-gutters bottom-buffer-30',
        'element' => 'div',
        'show_label' => '0',
        'label_element' => 'div',
        'attributes' => '',
      ),
    ),
  );
  $field_groups['group_photo_grid|paragraphs_item|sasg_photo_grid|default'] = $field_group;

  $field_group = new stdClass();
  $field_group->disabled = FALSE; /* Edit this to true to make a default field_group disabled initially */
  $field_group->api_version = 1;
  $field_group->identifier = 'group_photo_single|paragraphs_item|sasg_photo_grid_item|default';
  $field_group->group_name = 'group_photo_single';
  $field_group->entity_type = 'paragraphs_item';
  $field_group->bundle = 'sasg_photo_grid_item';
  $field_group->mode = 'default';
  $field_group->parent_name = '';
  $field_group->data = array(
    'label' => 'Single photo',
    'weight' => '0',
    'children' => array(
      0 => 'field_sasg_grid_photo',
      1 => 'group_caption',
    ),
    'format_type' => 'html-element',
    'format_settings' => array(
      'label' => 'Single photo',
      'instance_settings' => array(
        'id' => '',
        'classes' => 'photo-single',
        'element' => 'div',
        'show_label' => '0',
        'label_element' => 'div',
        'attributes' => '',
      ),
    ),
  );
  $field_groups['group_photo_single|paragraphs_item|sasg_photo_grid_item|default'] = $field_group;

  // Translatables
  // Included for use with string extractors like potx.
  t('Caption');
  t('Photo grid');
  t('Photo grid item');
  t('Photo grid item content');
  t('Single photo');

  return $field_groups;
}
