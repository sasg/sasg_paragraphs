<?php
/**
 * @file
 * sasg_paragraphs_photo_grid.features.field_instance.inc
 */

/**
 * Implements hook_field_default_field_instances().
 */
function sasg_paragraphs_photo_grid_field_default_field_instances() {
  $field_instances = array();

  // Exported field_instance:
  // 'paragraphs_item-sasg_photo_grid-field_sasg_photo_grid_items'.
  $field_instances['paragraphs_item-sasg_photo_grid-field_sasg_photo_grid_items'] = array(
    'bundle' => 'sasg_photo_grid',
    'default_value' => NULL,
    'deleted' => 0,
    'description' => '',
    'display' => array(
      'default' => array(
        'label' => 'hidden',
        'module' => 'paragraphs',
        'settings' => array(
          'view_mode' => 'sasg_multiple_paragraphs',
        ),
        'type' => 'paragraphs_view',
        'weight' => 0,
      ),
      'paragraphs_editor_preview' => array(
        'label' => 'above',
        'settings' => array(),
        'type' => 'hidden',
        'weight' => 0,
      ),
    ),
    'entity_type' => 'paragraphs_item',
    'fences_wrapper' => 'no_wrapper',
    'field_name' => 'field_sasg_photo_grid_items',
    'label' => 'Photos',
    'required' => 1,
    'settings' => array(
      'add_mode' => 'select',
      'allowed_bundles' => array(
        'sasg_carousel' => -1,
        'sasg_carousel_slide' => -1,
        'sasg_panel_group' => -1,
        'sasg_panel_item' => -1,
        'sasg_photo_grid' => -1,
        'sasg_photo_grid_item' => 'sasg_photo_grid_item',
        'sasg_tab_item' => -1,
        'sasg_tab_set' => -1,
        'sasg_text_field' => -1,
      ),
      'bundle_weights' => array(
        'sasg_carousel' => 2,
        'sasg_carousel_slide' => 3,
        'sasg_panel_group' => 4,
        'sasg_panel_item' => 5,
        'sasg_photo_grid' => 6,
        'sasg_photo_grid_item' => 7,
        'sasg_tab_item' => 8,
        'sasg_tab_set' => 9,
        'sasg_text_field' => 10,
      ),
      'default_edit_mode' => 'open',
      'title' => 'Photo',
      'title_multiple' => 'Photos',
      'user_register_form' => FALSE,
    ),
    'widget' => array(
      'active' => 0,
      'module' => 'paragraphs',
      'settings' => array(),
      'type' => 'paragraphs_embed',
      'weight' => 1,
    ),
  );

  // Exported field_instance:
  // 'paragraphs_item-sasg_photo_grid_item-field_sasg_grid_caption'.
  $field_instances['paragraphs_item-sasg_photo_grid_item-field_sasg_grid_caption'] = array(
    'bundle' => 'sasg_photo_grid_item',
    'default_value' => NULL,
    'deleted' => 0,
    'description' => '',
    'display' => array(
      'default' => array(
        'label' => 'hidden',
        'module' => 'text',
        'settings' => array(),
        'type' => 'text_default',
        'weight' => 1,
      ),
      'paragraphs_editor_preview' => array(
        'label' => 'above',
        'settings' => array(),
        'type' => 'hidden',
        'weight' => 0,
      ),
      'sasg_multiple_paragraphs' => array(
        'label' => 'hidden',
        'module' => 'text',
        'settings' => array(),
        'type' => 'text_default',
        'weight' => 1,
      ),
    ),
    'entity_type' => 'paragraphs_item',
    'fences_wrapper' => 'no_wrapper',
    'field_name' => 'field_sasg_grid_caption',
    'label' => 'Caption',
    'required' => 0,
    'settings' => array(
      'text_processing' => 0,
      'user_register_form' => FALSE,
    ),
    'widget' => array(
      'active' => 1,
      'module' => 'text',
      'settings' => array(
        'size' => 60,
      ),
      'type' => 'text_textfield',
      'weight' => 2,
    ),
  );

  // Exported field_instance:
  // 'paragraphs_item-sasg_photo_grid_item-field_sasg_grid_photo'.
  $field_instances['paragraphs_item-sasg_photo_grid_item-field_sasg_grid_photo'] = array(
    'bundle' => 'sasg_photo_grid_item',
    'deleted' => 0,
    'description' => '',
    'display' => array(
      'default' => array(
        'label' => 'hidden',
        'module' => 'image_formatter_link_to_image_style',
        'settings' => array(
          'image_class' => '',
          'image_link_style' => 'sasg_photo_grid_large',
          'image_style' => 'sasg_photo_grid_large',
          'link_class' => '',
          'link_rel' => '',
        ),
        'type' => 'image_formatter_link_to_image_style',
        'weight' => 1,
      ),
      'paragraphs_editor_preview' => array(
        'label' => 'above',
        'settings' => array(),
        'type' => 'hidden',
        'weight' => 0,
      ),
      'sasg_multiple_paragraphs' => array(
        'label' => 'hidden',
        'module' => 'image_formatter_link_to_image_style',
        'settings' => array(
          'image_class' => '',
          'image_link_style' => 'sasg_photo_grid_large',
          'image_style' => 'sasg_photo_grid_thumb',
          'link_class' => '',
          'link_rel' => '',
        ),
        'type' => 'image_formatter_link_to_image_style',
        'weight' => 2,
      ),
    ),
    'entity_type' => 'paragraphs_item',
    'fences_wrapper' => 'no_wrapper',
    'field_name' => 'field_sasg_grid_photo',
    'label' => 'Photo',
    'required' => 1,
    'settings' => array(
      'alt_field' => 1,
      'default_image' => 0,
      'file_directory' => '',
      'file_extensions' => 'png gif jpg jpeg',
      'max_filesize' => '',
      'max_resolution' => '',
      'min_resolution' => '',
      'title_field' => 0,
      'user_register_form' => FALSE,
    ),
    'widget' => array(
      'active' => 1,
      'module' => 'image',
      'settings' => array(
        'preview_image_style' => 'thumbnail',
        'progress_indicator' => 'throbber',
      ),
      'type' => 'image_image',
      'weight' => 1,
    ),
  );

  // Translatables
  // Included for use with string extractors like potx.
  t('Caption');
  t('Photo');
  t('Photos');

  return $field_instances;
}
