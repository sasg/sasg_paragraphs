<?php
/**
 * @file
 * sasg_paragraphs_photo_grid.features.inc
 */

/**
 * Implements hook_ctools_plugin_api().
 */
function sasg_paragraphs_photo_grid_ctools_plugin_api($module = NULL, $api = NULL) {
  if ($module == "field_group" && $api == "field_group") {
    return array("version" => "1");
  }
}

/**
 * Implements hook_image_default_styles().
 */
function sasg_paragraphs_photo_grid_image_default_styles() {
  $styles = array();

  // Exported image style: sasg_photo_grid_large.
  $styles['sasg_photo_grid_large'] = array(
    'label' => 'Photo Grid Large',
    'effects' => array(
      1 => array(
        'name' => 'image_scale',
        'data' => array(
          'width' => 900,
          'height' => 900,
          'upscale' => 0,
        ),
        'weight' => 1,
      ),
    ),
  );

  // Exported image style: sasg_photo_grid_thumb.
  $styles['sasg_photo_grid_thumb'] = array(
    'label' => 'Photo Grid Thumb',
    'effects' => array(
      2 => array(
        'name' => 'focal_point_scale_and_crop',
        'data' => array(
          'width' => 480,
          'height' => 360,
          'focal_point_advanced' => array(
            'shift_x' => '',
            'shift_y' => '',
          ),
        ),
        'weight' => 1,
      ),
    ),
  );

  return $styles;
}

/**
 * Implements hook_paragraphs_info().
 */
function sasg_paragraphs_photo_grid_paragraphs_info() {
  $items = array(
    'sasg_photo_grid' => array(
      'name' => 'Photo grid',
      'bundle' => 'sasg_photo_grid',
      'locked' => '1',
    ),
    'sasg_photo_grid_item' => array(
      'name' => 'Photo',
      'bundle' => 'sasg_photo_grid_item',
      'locked' => '1',
    ),
  );
  return $items;
}
