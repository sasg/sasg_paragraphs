(function ($) {
  // gallery
  $(document).ready(function() {
    $('.photo-grid').each(function() {
      $(this).magnificPopup({
        delegate: 'a',
        type: 'image',
        tLoading: 'Loading image #%curr%...',
        mainClass: 'mfp-img-mobile',
        gallery: {
          enabled: true,
          navigateByImgClick: true,
          preload: [0,1]
        },
        image: {
          tError: '<a href="%url%">The image #%curr%</a> could not be loaded.',
          titleSrc: function(item) {
            if ( item.el.siblings('.photo-grid-caption').html() ) {
              return item.el.siblings('.photo-grid-caption').html();
            } else {
              return '';
            }
          }
        }
      });
    });
    // single image
    $('.photo-single').magnificPopup({
      delegate: 'a',            
      type: 'image',
      closeOnContentClick: true,
      fixedContentPos: true,
      mainClass: 'mfp-img-mobile',
      image: {
        verticalFit: true,
        tError: '<a href="%url%">The image #%curr%</a> could not be loaded.',
        titleSrc: function(item) {
          if ( item.el.siblings('.photo-grid-caption').html() ) {
            return item.el.siblings('.photo-grid-caption').html();
          } else {
            return '';
          }
        }
      }
    });
  });
}(jQuery));
