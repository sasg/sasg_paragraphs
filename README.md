# SASG Paragraphs #

### Paragraph Bundles ###

* Card
* Card Deck
* Carousel
* Carousel Slide
* Panel group
* Panel
* Partner
* Partner group
* Photo grid
* Photo
* Statistic
* Statistic group
* Tab Section
* Tabs
* Table
* Text
* Video
* View

### Required libraries ###

The Photo grid bundle requires Magnific Popup. Download the library at https://github.com/dimsemenov/Magnific-Popup, rename the directory to magnific-popup and add it to libraries.

The Statistic group bundle requires Owl Carousel. Download the library at https://github.com/OwlCarousel2/OwlCarousel2, rename the directory to owlcarousel and add it to libraries.
